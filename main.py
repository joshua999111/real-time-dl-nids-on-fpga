import os
import sys
import torch
import numpy as np
from dataset_interface.dataset_interface import DatasetInterface
from dataset_interface.dataset_statistics import FlowDensityCounter

import matplotlib.pyplot as plt

def dataset_statistics_func():

    stor_path = 'CICIDS2017_base_PCCN_hepa.json'
    ctr = FlowDensityCounter(path=stor_path, load=True)
    print(ctr.get_overall_percentages())


if __name__ == '__main__':

    dataset_statistics_func()
    sys.exit(0)
    #from quantization.brevitas.CNV import cnv_w2a2
    # from quantization.brevitas.custom_networks import qbase2dcnn
    # net = qbase2dcnn(num_classes=10)
    #
    # dummy_input = np.zeros((2, 1, 22, 22))
    # net.forward(torch.from_numpy(dummy_input).float())
    # sys.exit(0)

    # from networks.networks import BaseCNN1D
    #
    # net = BaseCNN1D(num_classes=10, input_size=484)
    #
    # dummy_input = np.zeros((2, 1, 484))
    # net.forward(torch.from_numpy(dummy_input).float())
    # sys.exit(0)

    from learning.resource_requirements import get_number_parameters_net, get_flops_network, \
        format_macs, format_storage_size, num_parameters_to_storage_size
    from networks.networks import BaseCNN1D, BaseCNN2D, HAST2IDS, ParallelCrossCNN

    # BaseCNN1D:
    pnum, dtype = get_number_parameters_net(BaseCNN1D(num_classes=10, input_size=484), verbose=False)
    storage = format_storage_size(num_parameters_to_storage_size(pnum, dtype))
    macs = get_flops_network(BaseCNN1D(num_classes=10, input_size=484), input_shape=(1, 1, 484))[0]
    macs = format_macs(macs)
    print('BaseCNN1D: {} parameters - {} - {}'.format(pnum, storage, macs))

    # BaseCNN2D:
    pnum, dtype = get_number_parameters_net(BaseCNN2D(num_classes=10, head_payload=True), verbose=False)
    storage = format_storage_size(num_parameters_to_storage_size(pnum, dtype))
    macs = get_flops_network(BaseCNN2D(num_classes=10, head_payload=True), input_shape=(1, 1, 22, 22))[0]
    macs = format_macs(macs)
    print('BaseCNN2D: {} parameters - {} - {}'.format(pnum, storage, macs))

    # PCCN:
    pnum, dtype = get_number_parameters_net(ParallelCrossCNN(num_classes=10, head_payload=True), verbose=False)
    storage = format_storage_size(num_parameters_to_storage_size(pnum, dtype))
    macs = get_flops_network(ParallelCrossCNN(num_classes=10, head_payload=True), input_shape=(1, 1, 22, 22))[0]
    macs = format_macs(macs)
    print('PCCN: {} parameters - {} - {}'.format(pnum, storage, macs))

    # HAST-II:
    pnum, dtype = get_number_parameters_net(HAST2IDS(num_classes=10), verbose=False)
    storage = format_storage_size(num_parameters_to_storage_size(pnum, dtype))
    macs = get_flops_network(HAST2IDS(num_classes=10), input_shape=(1, 6, 256, 100))[0]
    macs = format_macs(macs)
    print('HAST-II: {} parameters - {} - {}'.format(pnum, storage, macs))

    #print(get_number_parameters_net())
    #print(get_number_parameters_net(ParallelCrossCNN(num_classes=10, head_payload=True), verbose=False))
    #print(get_number_parameters_net())

    #print()
    #print(get_flops_network()
    #print(get_flops_network(ParallelCrossCNN(num_classes=10, head_payload=True), input_shape=(1, 1, 22, 22)))
    #print(get_flops_network()


    # stream1 = os.path.join('/', 'media', 'laurens', 'Preprocessed datasets', 'Datasets',
    #                        'CICIDS2017_full_stream_1000_5_PCCN_hepa')
    # interface = DatasetInterface(stream1)
    # counter = FlowDensityCounter(interface=interface, verbose=True)
    # counter.store('.', 'CICIDS2017_full_stream_1000_5_PCCN_hepa')

    #stream1 = os.path.join('/', 'media', 'laurens', 'Preprocessed datasets', 'Datasets',
    #                       'UNSW_NB15_base_PCCN_hepa')
    #interface = DatasetInterface(stream1)
    #counter = FlowDensityCounter(interface=interface, verbose=True)
    #counter.store('.', 'UNSW_NB15_base_PCCN_hepa')

    # stream1 = os.path.join('/', 'media', 'laurens', 'Preprocessed datasets', 'Datasets',
    #                        'CICIDS2017_base_PCCN_hepa')
    # interface = DatasetInterface(stream1)
    # counter = FlowDensityCounter(interface=interface, verbose=True)
    # counter.store('.', 'CICIDS2017_base_PCCN_hepa')
    # # +
    #
    # sys.exit(0)

    #stream1 = os.path.join('/', 'media', 'laurens', 'Preprocessed datasets', 'Datasets', 'CICIDS2017_full_stream_50_5_PCCN_hepa')
    #stream2 = os.path.join('/', 'media', 'laurens', 'Preprocessed datasets', 'Datasets', 'CICIDS2017_full_stream_100_5_PCCN_hepa')

    # interface1 = DatasetInterface(stream1)
    # counter1 = FlowDensityCounter(interface=interface1, verbose=True)
    # counter1.store('.', 'CICIDS2017_full_stream_50_5_PCCN_hepa')
    #
    # interface2 = DatasetInterface(stream2)
    # counter2 = FlowDensityCounter(interface=interface2, verbose=True)
    # counter2.store('.', 'CICIDS2017_full_stream_100_5_PCCN_hepa')

    # stream1 = os.path.join('/', 'media', 'laurens', 'Preprocessed datasets', 'Datasets',
    #                        'CICIDS2017_full_stream_10_5_PCCN_hepa')
    #
    # interface1 = DatasetInterface(stream1)
    # counter1 = FlowDensityCounter(interface=interface1, verbose=True)
    # counter1.store('.', 'CICIDS2017_full_stream_10_5_PCCN_hepa')
    #
    # sys.exit()

    # net = cnv_w2a2(num_classes=10, in_channels=1)
    # dummy_input = np.zeros((2, 1, 22, 22))
    # net.forward(torch.from_numpy(dummy_input).float())
    # sys.exit(0)
    #testdataset = os.path.join('/', 'home', 'laurens', 'Experiment', 'UNSW_NB15_base_PCCN_hepa')

    #interface = DatasetInterface(testdataset)

    #counter = FlowDensityCounter(interface, verbose=True)

    #print(counter.get_overall_stats())
    #counter.store('.', 'test')
    #print(FlowDensityCounter(path=os.path.join('.', 'test.json'), load=True).get_overall_stats())

    #stream1 = os.path.join('/', 'home', 'laurens', 'Experiment', 'UNSW_NB15_stream_1_5_PCCN_hepa')
    #stream2 = os.path.join('/', 'home', 'laurens', 'Experiment', 'UNSW_NB15_stream_2_5_PCCN_hepa')

    #interface1 = DatasetInterface(stream1)
    #counter1 = FlowDensityCounter(interface=interface1, verbose=True)
    #counter1.store('.', 'UNSW_NB15_stream_1_5_PCCN_hepa')

    #interface2 = DatasetInterface(stream2)
    #counter2 = FlowDensityCounter(interface=interface2, verbose=True)
    #counter2.store('.', 'UNSW_NB15_stream_2_5_PCCN_hepa')

    #stream3 = os.path.join('/', 'home', 'laurens', 'Experiment', 'UNSW_NB15_stream_100_5_PCCN_hepa')
    #interface3 = DatasetInterface(stream3)
    #counter3 = FlowDensityCounter(interface=interface3, verbose=True)
    #counter3.store('.', 'UNSW_NB15_stream_100_5_PCCN_hepa')

    # print(FlowDensityCounter(path=os.path.join('.', 'test.json'), load=True).get_overall_count())
    # print(FlowDensityCounter(path=os.path.join('.', 'test.json'), load=True).get_overall_percentages())
    # print(FlowDensityCounter(path=os.path.join('.', 'UNSW_NB15_stream_1_5_PCCN_hepa.json'),
    #                          load=True).get_overall_count())
    # print(FlowDensityCounter(path=os.path.join('.', 'UNSW_NB15_stream_1_5_PCCN_hepa.json'),
    #                          load=True).get_overall_percentages())
    # print(FlowDensityCounter(path=os.path.join('.', 'UNSW_NB15_stream_2_5_PCCN_hepa.json'),
    #                          load=True).get_overall_count())
    # print(FlowDensityCounter(path=os.path.join('.', 'UNSW_NB15_stream_2_5_PCCN_hepa.json'),
    #                          load=True).get_overall_percentages())
    # print(FlowDensityCounter(path=os.path.join('.', 'UNSW_NB15_stream_100_5_PCCN_hepa.json'),
    #                          load=True).get_overall_count())
    # print(FlowDensityCounter(path=os.path.join('.', 'UNSW_NB15_stream_100_5_PCCN_hepa.json'),
    #                          load=True).get_overall_percentages())

    # for n_streams in [1]:
    #     stream = os.path.join('/', 'home', 'laurens', 'Experiment', 'CICIDS2017_stream_{}_5_PCCN_hepa'.format(n_streams))
    #     interface = DatasetInterface(stream)
    #     counter = FlowDensityCounter(interface=interface, verbose=True)
    #     counter.store('.', 'CICIDS2017_stream_{}_5_PCCN_hepa'.format(n_streams))
    #
    #     print(counter.get_overall_count())
    #     print(counter.get_overall_percentages())

    # for n_streams in [100, 50, 10, 5, 1]:
    #     counter = FlowDensityCounter(path=os.path.join('.', 'CICIDS2017_stream_{}_5_PCCN_hepa.json'.format(n_streams)),
    #                                  load=True)
    #     print(counter.get_overall_count())
    #     print(counter.get_overall_percentages())
