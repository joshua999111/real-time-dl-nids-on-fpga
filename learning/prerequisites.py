from torchvision import transforms
import torch.optim as optim
import numpy as np

import networks.networks as nets
from experiment_metadata.enums import Model, Optimizer
from setup_dataset import BaseTrafficDataset, NormalizeTraffic, ToTensor, \
   ToTensorOHE, NormalizeSessionBasedFeatures, ToTensor1D, ReduceTo1D, CompressedTrafficDataset, AggregatePackets

from quantization.brevitas import qbasecnn2d as QBASE
from dataset_interface.enums import FeatureStrategy


def get_net(experiment):

    if Model[experiment.model] == Model.SimpleNet:
        return nets.SimpleNet(num_classes=len(experiment.get_labels()))
    elif Model[experiment.model] == Model.PCCN:
        if experiment.dataset_interface.get_feature_type() == 'hepa':
            hepa = True
        else:
            hepa = False
        return nets.ParallelCrossCNN(num_classes=len(experiment.get_labels()), head_payload=hepa)
    elif Model[experiment.model] == Model.PCCNN:
        x = experiment.dataset_interface.get_feature_type()
        if x == 'hepa':
            hepa = True
        else:
            hepa = False
        return nets.CROSS_CNN(num_classes=len(experiment.get_labels()), head_payload=hepa)
    elif Model[experiment.model] == Model.HAST_II:
        return nets.HAST2IDS(num_classes=len(experiment.get_labels()))
    elif Model[experiment.model] == Model.TiedSDA:
        return nets.TiedSDAClassifier(num_classes=len(experiment.get_labels()), shapes=experiment.shapes)
    elif Model[experiment.model] == Model.TCN:
        input_channels = 1  # 1-D byte vector of size n_packet * n_bytes
        n_layers = 8
        num_channels = [32] * n_layers  # Number of channels per layer
        kernel_size = 7
        return nets.TCN(input_size=input_channels, output_size=len(experiment.get_labels()),
                        num_channels=num_channels, kernel_size=kernel_size)
    elif Model[experiment.model] == Model.BaseCNN2D:
        if experiment.dataset_interface.get_feature_type() == 'hepa':
            hepa = True
        else:
            hepa = False
        return nets.BaseCNN2D(num_classes=len(experiment.get_labels()), head_payload=hepa)
    elif Model[experiment.model] == Model.BaseCNN2D2:
        if experiment.dataset_interface.get_feature_type() == 'hepa':
            hepa = True
        else:
            hepa = False
        return nets.BaseCNN2D2(num_classes=len(experiment.get_labels()), head_payload=hepa)
    elif Model[experiment.model] == Model.BaseCNN1D:
        return nets.BaseCNN1D(num_classes=len(experiment.get_labels()), input_size=22*22)
    elif Model[experiment.model] == Model.QBaseCNN2DAvgPool_W2A2:
        return QBASE.QBaseCNN2DAvgpool(num_classes=len(experiment.get_labels()), in_ch=1,
                                       weight_bit_width=2, act_bit_width=2)
    elif Model[experiment.model] == Model.QBaseCNN2DAvgPool_W4A4:
        return QBASE.QBaseCNN2DAvgpool(num_classes=len(experiment.get_labels()), in_ch=1,
                                       weight_bit_width=4, act_bit_width=4)
    elif Model[experiment.model] == Model.QBaseCNN2DAvgPool_W8A8:
        return QBASE.QBaseCNN2DAvgpool(num_classes=len(experiment.get_labels()), in_ch=1,
                                       weight_bit_width=8, act_bit_width=8)
    elif Model[experiment.model] == Model.QBaseCNN2DAvgPool_W16A16:
        return QBASE.QBaseCNN2DAvgpool(num_classes=len(experiment.get_labels()), in_ch=1,
                                       weight_bit_width=16, act_bit_width=16)

    else:
        return None

def get_dataset(experiment, usage):
    if experiment.dataset_interface.get_feature_strategy() == FeatureStrategy.HAST_II:
        transf = transforms.Compose([ToTensorOHE()])
        return CompressedTrafficDataset(experiment=experiment, usage=usage, transform=transf)
    elif experiment.dataset_interface.get_feature_strategy() == FeatureStrategy.SESSIONBASED:
        max = np.array(experiment.dataset_interface.get_features_max())
        min = np.array(experiment.dataset_interface.get_features_min())
        transf = transforms.Compose(
            [NormalizeSessionBasedFeatures(experiment.include_icmp(),
                                           max=max, min=min), ToTensor1D()]
        )
        return BaseTrafficDataset(experiment=experiment, usage=usage, transform=transf)
    elif Model[experiment.model] == Model.TCN:
        transf = transforms.Compose([AggregatePackets(),
                                     NormalizeTraffic(no_normalization=True),
                                     ToTensor1D(expand_dims=True)])
        return BaseTrafficDataset(experiment=experiment, usage=usage, transform=transf)
    elif (Model[experiment.model] == Model.BaseCNN1D): #or (Model[experiment.model] == Model.QBaseCNN1D):
        transf = transforms.Compose([NormalizeTraffic(no_normalization=True), ToTensor(), ReduceTo1D(2)])
        return BaseTrafficDataset(experiment=experiment, usage=usage, transform=transf)
    #elif (Model[experiment.model] == Model.QBaseCNN2dNP_W2A2) or (Model[experiment.model] == Model.QBaseCNN2dNP_W4A4):
    #    transf = transforms.Compose([NormalizeTraffic(max=255, min=0, use_minmax=True), ToTensor()])
    #    return BaseTrafficDataset(experiment=experiment, usage=usage, transform=transf)
    else:
        transf = transforms.Compose([NormalizeTraffic(no_normalization=True), ToTensor()])
        return BaseTrafficDataset(experiment=experiment, usage=usage, transform=transf)


def get_optimizer(optimizer):
    if optimizer == Optimizer.RMSprop:
        return optim.RMSprop
    elif optimizer == Optimizer.SGD:
        return optim.SGD
    else:
        return None


def get_input_shape(experiment):
    base_shape = experiment.dataset_interface.get_shape()
    if Model[experiment.model] == Model.SimpleNet:
        pass
    elif Model[experiment.model] == Model.PCCN:
        return tuple([1, 1] + list(base_shape))
    elif Model[experiment.model] == Model.PCCNN:
        return tuple([1, 1] + list(base_shape))
    elif Model[experiment.model] == Model.HAST_II:
        pass
    elif Model[experiment.model] == Model.TiedSDA:
        pass
    elif Model[experiment.model] == Model.TCN:
        pass
    elif Model[experiment.model] == Model.BaseCNN2D:
        pass
    elif Model[experiment.model] == Model.BaseCNN1D:
        pass
    elif Model[experiment.model] == Model.QBaseCNN2DAvgPool_W2A2:
        return tuple([1, 1] + list(base_shape))
    elif Model[experiment.model] == Model.QBaseCNN2DAvgPool_W4A4:
        return tuple([1, 1] + list(base_shape))
    elif Model[experiment.model] == Model.QBaseCNN2DAvgPool_W8A8:
        return tuple([1, 1] + list(base_shape))
    elif Model[experiment.model] == Model.QBaseCNN2DAvgPool_W16A16:
        return tuple([1, 1] + list(base_shape))

    # elif Model[experiment.model] == Model.CNV_W2A2:
    #     return tuple([1, 1] + list(base_shape))
    # elif Model[experiment.model] == Model.CNV_W1A1:
    #     return tuple([1, 1] + list(base_shape))
    # elif Model[experiment.model] == Model.QBaseCNN2D:
    #     return tuple([1, 1] + list(base_shape))
    # elif Model[experiment.model] == Model.QBaseCNN2D_W1A1:
    #     return tuple([1, 1] + list(base_shape))
    # elif Model[experiment.model] == Model.QBaseCNN2D_W2A2:
    #     return tuple([1, 1] + list(base_shape))
    # elif Model[experiment.model] == Model.QBaseCNN2dNP_W1A1:
    #     return tuple([1, 1] + list(base_shape))
    # elif Model[experiment.model] == Model.QBaseCNN2dNP_W2A2:
    #     return tuple([1, 1] + list(base_shape))
    # elif Model[experiment.model] == Model.QBaseCNN2dNP_W4A4:
    #     return tuple([1, 1] + list(base_shape))
    # elif Model[experiment.model] == Model.QBaseCNN2dAP:
    #     return tuple([1, 1] + list(base_shape))
    # elif Model[experiment.model] == Model.QBaseCNN2dMP1024_W2A2:
    #     return tuple([1, 1] + list(base_shape))
    # elif Model[experiment.model] == Model.TestDependencies:
    #     return tuple([1, 1] + list(base_shape))

    else:
        return None
    return None
