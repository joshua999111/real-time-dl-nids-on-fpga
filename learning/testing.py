import numpy as np
import torch
from torch.utils.data import DataLoader

from learning.prerequisites import get_net, get_dataset
from evaluation.evaluation import Evaluation


def test_experiment(experiment, verbose=True):

    if verbose:
        print('Testing experiment \'{}\''.format(experiment.name))

    # Define device:
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    neural_net = get_net(experiment)
    neural_net.load_state_dict(torch.load(experiment.best_model_path))

    # Call eval() to deal with batch norm layers
    # Reason: https://discuss.pytorch.org/t/error-expected-more-than-1-value-per-channel-when-training/26274/2
    neural_net.eval()
    neural_net.to(device)

    # Get training set
    testset = get_dataset(experiment=experiment, usage='test')

    # Data loader
    testloader = DataLoader(testset, batch_size=256, shuffle=True, num_workers=8)
    test_visual_batches = len(testloader) // 20

    # Run tests
    predictions = []
    actual = []

    with torch.no_grad():
        # Visualization contstants
        test_size = len(testset)
        for i, data in enumerate(testloader, 0):

            # get the inputs; data is a list of [inputs, labels]
            inputs = data["item"].to(device)
            labels = data["label"].to(device)
            outputs = neural_net(inputs.float())
            _, predicted = torch.max(outputs, 1)

            predictions += predicted
            actual += labels

            if i % test_visual_batches == (test_visual_batches - 1):  # print every test_visual_batches mini-batches
                print('{} / {}'.format((i + 1) * testloader.batch_size, test_size), end='\r')

    print('Converting to numpy arrays')
    actual = torch.stack(actual, 0).cpu().numpy()
    predictions = torch.stack(predictions, 0).cpu().numpy()

    # Store results as text file and json file
    evaluation = Evaluation(actual, predictions, labels=experiment.get_labels(), frac=False)
    eval_text = evaluation.classification_report(output_dict=False)
    experiment.store_results_text(eval_text)
    eval_dict = evaluation.classification_report(output_dict=True)
    dict_numpy_to_python(eval_dict)
    experiment.store_results_json(eval_dict)
    cm_dict = evaluation.cm_as_dict()
    experiment.store_results_cm(cm_dict)

    experiment.status['test']['complete'] = True
    experiment.status['test']['accuracy'] = eval_dict['accuracy']
    experiment.save()

    print(eval_text)
    print(evaluation.evaluate_ids(experiment.dataset_interface.get_normal_class()))


def dict_numpy_to_python(dictionary):
    """
    For the given dictionary, turn each numpy data type into its corresponding Python data type.
    :param dictionary: Input dictionary for which the data types will be transformed
    :rtype dictionary: ``dict``
    :return: None
    """
    for key, item in dictionary.items():
        _dict_numpy_to_python(dictionary, key, item)


def _dict_numpy_to_python(base, key, item):
    """
    Turn the numpy numbers in the provided item of the provided base dictionary with the corresponding provided key into
     Python numbers.
    :param base: Base dictionary containing the provided item for the provided key.
    :param key: Key corresponding with the provided item
    :param item: Provided item containing (a) number with a Numpy type.
    :type base: ``dict``
    :type key: ``str``
    :type item: ``dict`` or ``number``
    :return: None
    """
    if isinstance(item, dict):
        for next_key, next_item in item.items():
            _dict_numpy_to_python(item, next_key, next_item)
    else:
        base[key] = numpy_to_python_data_type(item)


def numpy_to_python_data_type(data):
    """
    Transform a Numpy data type into a regular data type serializable by JSON.

    :param data: Data to have its type transformed if possible
    :type data: ``numpy.int64`` or ``numpy.float64`` will be transformed
    :return: Transformed data if applicable, unchanged input data otherwise
    :rtype: ``int`` for ``numpy.int64``, ``float`` for ``numpy.float64``
    """
    if isinstance(data, np.int64):
        return int(data)
    elif isinstance(data, np.float64):
        return float(data)
    else:
        return data
