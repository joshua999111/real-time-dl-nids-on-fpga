import torch
import numpy as np

from thop import profile
from learning.prerequisites import get_net, get_input_shape
from torch import float64, float32, float16, int64, int32, int16, int8


def get_number_parameters_experiment(experiment):
    """
    Get the number of model parameters for the specified model.
    :param experiment: The experiment for which the number of trainable parameters is required
    :return: Tuple of the number of parameters and the datatype of those parameters
    :rtype ``tuple`` of (``int``, ``torch.dtype``)
    """

    model = get_net(experiment)

    #return sum(p.numel() for p in model.parameters() if p.requires_grad), next(model.parameters()).dtype
    # for parameter in model.parameters():
    #     print(parameter)
    return get_number_parameters_net(model)


def get_number_parameters_net(network, verbose=True):
    """

    :param network:
    :return:
    """
    plist = []
    for name, p in network.named_parameters():
        if p.requires_grad:
            plist.append(p.numel())
            if verbose:
                print('{}: number of elements: {}'.format(name, p.numel()))
    return sum(plist), next(network.parameters()).dtype


def get_flops_network(network, input_shape):

    sample_input = torch.randn(input_shape)
    macs, params = profile(network, inputs=(sample_input,))
    return macs, params


def get_flops(experiment):

    model = get_net(experiment)
    input_shape = get_input_shape(experiment)
    return get_flops_network(model, input_shape)


def num_parameters_to_storage_size(numel, dtype):
    """
    For the given number of elements of the given data type, calculate the number of storage bytes required.

    :param numel: Number of parameters.
    :param dtype: Datatype of the parameters
    :type numel: ``int``
    :type dtype: ``torch.dtype``
    :return: Required number of storage bytes for the specified number of elements of the specified data type.
    :rtype: ``int``
    """
    if (dtype == float64) or (dtype == int64):
        nbpp = 8
    elif (dtype == float32) or (dtype == int32):
        nbpp = 4
    elif (dtype == float16) or (dtype == int16):
        nbpp = 2
    elif dtype == int8:
        nbpp = 1
    else:
        raise NotImplementedError('Unrecognized data type', dtype)

    return nbpp * numel


def format_storage_size(n_bytes, base=1024):
    """
    Express the provided number of bytes in a readable format.

    :param n_bytes: Number of bytes.
    :param base: Base that will be used for size calculation, typically 1000 or 1024. Default is binary base (1024).
    :type n_bytes: ``int``
    :type base: ``int``
    :return: String giving the number of GB, MB, kB, B, depending on what is most suitable
    :rtype: ``str``
    """
    if n_bytes > pow(base, 3):
        label = 'GB'
        size = n_bytes / pow(base, 3)
    elif n_bytes > pow(base, 2):
        label = 'MB'
        size = n_bytes / pow(base, 2)
    elif n_bytes > pow(base, 1):
        label = 'kB'
        size = n_bytes / pow(base, 1)
    else:
        label = 'B'
        size = n_bytes
    return '{} {}'.format(np.round(size, 3), label)


def format_macs(macs):
    """
    Express the provided number of Multiply-and-ACcumulate operations more legible.

    :param macs: Number of bytes.
    :type macs: ``int``
    :return: String giving the number of GMACs, MMACs, kMACs, MACs, depending on what is most suitable
    :rtype: ``str``
    """
    base = 1000
    if macs > pow(base, 3):
        label = 'GMACs'
        size = macs / pow(base, 3)
    elif macs > pow(base, 2):
        label = 'MMACs'
        size = macs / pow(base, 2)
    elif macs > pow(base, 1):
        label = 'kMACs'
        size = macs / pow(base, 1)
    else:
        label = 'MACs'
        size = macs
    return '{} {}'.format(np.round(size, 3), label)