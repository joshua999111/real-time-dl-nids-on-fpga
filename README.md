# Real-Time Deep Learning Network Intrusion Detection System on FPGA

## Overview
This repository contains the code used to:

1) Create a quantized CNN using Quantization-Aware Training (QAT) provided in the Brevitas library.

2) The Jupyter Notebook used to transform the generated quantized network to hardware with FINN.

Below is a list of instructions to achieve both goals using this code.

## Usage

### Citation
Please cite the following publication if you use this code or if you find this research relevant:
```
@InProceedings{10.1007/978-3-030-81645-2_9,
    author="Le Jeune, Laurens and Goedem{\'e}, Toon and Mentens, Nele",
    editor="Zhou, Jianying and Ahmed, Chuadhry Mujeeb and Batina, Lejla and Chattopadhyay, Sudipta and Gadyatskaya, Olga and Jin, Chenglu and  Lin, Jingqiang and Losiouk, Eleonora and Luo, Bo and Majumdar, Suryadipta and Maniatakos, Mihalis and Mashima, Daisuke and Meng, Weizhi and Picek, Stjepan and Shimaoka, Masaki and Su, Chunhua and Wang, Cong",
    title="Towards Real-Time Deep Learning-Based Network Intrusion Detection on FPGA",
    booktitle="Applied Cryptography and Network Security Workshops",
    year="2021",
    publisher="Springer International Publishing",
    address="Cham",
    pages="133--150",
    isbn="978-3-030-81645-2"
}
```

As this software is supported by [Brevitas](https://github.com/Xilinx/brevitas) and [FINN](https://github.com/Xilinx/finn), please consider citing those sources accordingly when using this software.

### Requirements

1. Dataset with interface as provided in [this repository](https://gitlab.com/EAVISE/raw-traffic-nids).
2. Storage directory to store training metadata and results
3. FINN environment (this version was tested for FINN v0.5b)
4. PYNQ-Z2 FPGA to run experiments

Contact me at *laurens.lejeune@kuleuven.be* to get access to a sample dataset.

### Training a quantized network
The command for training a network is the following:

```
python learning_cli.py --root path/to/storage/dir/ learning --dataset path/to/dataset_interface/ --name 'name_of_experiment' --model name_of_model --n_epochs number_of_epochs --continue_tr True
```

The following quantized models are available for training:

+ QBaseCNN2DAvgPool_W2D2
+ QBaseCNN2DAvgPool_W4D4
+ QBaseCNN2DAvgPool_W8D8
+ QBaseCNN2DAvgPool_W16D16

Running the same command again for a trained network will initiate testing and evaluation. In this case only providing the storage directory, the dataset and the name of the experiment should suffice:

```
python learning_cli.py --root path/to/storage/dir/ learning --dataset path/to/dataset_interface/ --name 'name_of_experiment'
```

### Exporting a trained network

Once trained, a model can be exported to an ONNX representation suitable for FINN using the following command:

```
python learning_cli.py --root path/to/storage/dir/ export --representation onnx --name 'name_of_experiment' --destination /path/to/storage/dir/model_name.onnx
```

Using this command, the exported model will be stored in the /path/to/storage/dir/ directory as *model_name.onnx*.

Similarly, it is possible to export evaluation samples that allow for evaluation of the model on the FPGA:

```
python learning_cli.py --root path/to/storage/dir/ export --representation valid_samples --name 'name_of_experiment' --destination /path/to/storage/dir/ --n_samples number_of_samples
```
This will store two files, *name_of_experiments-eval.npy* and *name_of_experiments-data.npy*, in the /path/to/storage/dir/ directory. They *data* file contains the samples that can be used for evaluation, while the *eval* file contains the right output. For each class, up to *number_of_samples* samples are included.

### Translating a model using FINN
The Jupyter notebook *builder.ipynb* can be used inside the FINN environment to generate hardware from the ONNX model *model_name.onnx*. It is advisable to upload said model alongside the notebook.


### Experimentation on PYNQ-Z2
Running the notebook will produce a directory that can be uploaded to a PYNQ-Z2 board for testing. The structure of this directory, provided as the *deploy* output directory, is as follows:
```
deploy/
├── bitfile/
│   ├── finn-accel.bit
│   └── finn-accel.hwh
└── driver/
    ├── finn/
    ├── runtime_weights/
    ├── driver.py
    ├── driver_base.py
    └── validate.py

```
In order to actually run a test, this directory needs to be copied to the FPGA. One way to do this is:

```
scp -r /path/to/deploy/dir/on/system/ xilinx@ip.address.of.fpga:/home/xilinx/
```
The password is *xilinx*. Follow the [set-up guide of the PYNQ-Z2](https://pynq.readthedocs.io/en/v2.6.1/getting_started/pynq_z2_setup.html) if you encounter issues with this step. Then use SSH to connect with the device and access its system through a terminal:

```
ssh -l xilinx ip.address.of.fpga
```
Once again, the password is *xilinx*.
Using these files on the PYNQ-Z2, it is already possible to evaluate the throughput of the system. Running:

```
sudo python3 driver.py --exec_mode throughput_test --batchsize 100
```
will, after some time, produce the *nw_metrics.txt* file that contains the required metrics. If might be necessary to change the default filename in the *driver.py* file.
If it is desirable to evaluate the detection performance of the system, some additional files need to be added. Both files enclosed within the *evaluation_on_fpga* directory in this repository will allow for detection performance assesment. Note that for full functionality, it is necessary to install *sklearn* on the PYNQ-Z2. This can be avoided by commenting out the final line in the *inference_validation.py* file, as well as the import of the Evaluation class at the top of the same file. Similarly, it might be required to modify the settings in the *__main__* function of the *inference_validation.py* file corresponding with the actual name and location of the bitfile. Finally, the *data* and *eval* NPY files generated in the export step will be needed to provide samples to the machine learning model. By then running:

```
sudo python3 inference_validation.py
```

the detection performance of the machine learning model can be verified.

## AITIA Demo
Enclosed within the *aitia_demo* subdirectory in this repository are the files used to give a demo at a webinar. These files are the result from training with Brevitas and transforming with FINN and can be uploaded to a PYNQ-Z2 for testing. This model has a throughput of about 9635fps for a clock frequency of 100MHz, as reported in the paper cited above. Following the steps in the previous section will allow for validating the design when running it on the PYNQ-Z2 board.
