import os
import sys
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
import matplotlib.pyplot as plt
import torch
import numpy as np
import warnings
from dataset_interface.enums import DataType


class ToTensorOHE(object):
    """Convert ndarrays in sample to Tensors."""

    def __call__(self, sample):
        #image, label = sample['image'], sample['label']
        item = sample['item']

        return {'item': torch.from_numpy(item), 'label': sample['label']}


class ToTensor(object):
    """Convert ndarrays in sample to Tensors."""

    def __call__(self, sample):
        #image, label = sample['image'], sample['label']
        item = sample['item']

        # First, turn n x m image into a n x m x c image (c = 1 for grayscale)
        # For axis=0: Create 1 x n x m
        # For axis=1: Create n x 1 x m
        # For axis=2: Create n x m x 1
        item = np.expand_dims(item, axis=2)

        # Now swap the axes:
        # numpy image: H x W x C
        # torch image: C X H X W
        item = item.transpose((2, 0, 1))

        return {'item': torch.from_numpy(item), 'label': sample['label']}


class ReduceTo1D(object):
    """ Convert nD tensors to 1D tensors"""

    def __init__(self, base_dim):
        self.base_dim = base_dim

    def __call__(self, sample):
        item = sample['item']
        item = item.view(item.shape[0], -1)

        return {'item': item, 'label': sample['label']}


class ToTensor1D(object):
    """Convert ndarrays in sample to Tensors."""

    def __init__(self, **kwargs):
        # If True, create [1 x n] Tensor instead of a [n] Tensor.
        self.expand_dims = kwargs.get('expand_dims', False)

    def __call__(self, sample):

        if not self.expand_dims:
            return {'item': torch.from_numpy(sample['item']), 'label': sample['label']}
        else:
            return {'item': torch.from_numpy(np.expand_dims(sample['item'], axis=0)), 'label': sample['label']}


class AggregatePackets(object):
    """
    Aggregate separate packet vectors into one large vector. If for example 3 packets containing the bytes 'ABC', 'DEF',
    '987' exist, this function would turn [[A,B,C], [D,E,F], [9,8,7]] into [A,B,C,D,E,F,9,8,7].
    """
    def __call__(self, sample):
        return {'item': sample['item'].flatten(), 'label': sample['label']}


class NormalizeTraffic(object):

    def __init__(self, **kwargs):
        self.mean = kwargs.get('mean', 0)
        self.std = kwargs.get('std', 1)

        self.min = kwargs.get('min', 0)
        self.max = kwargs.get('max', 1)

        self.use_mean_normalization = kwargs.get('use_mean', False)
        self.use_minmax_normalization = kwargs.get('use_minmax', False)
        self.use_no_normalization = kwargs.get('no_normalization', False)
        self.divide_by_byte = kwargs.get('divide_by_byte', False)
        if not self.use_no_normalization:
            if not (self.use_mean_normalization or self.use_minmax_normalization):
                raise Warning('NO NORMALIZATION APPROACH HAS BEEEN SELECTED')
            elif self.use_minmax_normalization and self.use_mean_normalization:
                raise Warning('MULTIPLE NORMALIZATION APPROACHES HAVE BEEN SELECTED')

    def __call__(self, sample):
        item = sample['item']

        if self.use_mean_normalization:
            subtractor = self.mean
            divisor = self.std
        elif self.use_minmax_normalization:
            subtractor = self.min
            divisor = (self.max - self.min)
        elif self.divide_by_byte:
            subtractor = 0
            divisor = 255
        else:
            subtractor = 0
            divisor = 1
        #mean = np.mean(item)
        #std = np.std(item)

        #print(mean, std)
        #if mean==0.0 and std==0.0:
        #    print("help")

        warnings.filterwarnings('error')
        with np.errstate(divide='warn'):
            try:
                result = (item - subtractor) / divisor
            except RuntimeWarning:
                print(divisor)
        #print("From old shape {} to new shape {}".format(item.shape, result.shape))
        return {'item': result, 'label': sample['label']}


class NormalizeSessionBasedFeatures(object):
    """
    When normalizing traffic bytes between 0 and 1, simply dividing by 255 does the trick
    """

    def __init__(self, include_icmp, min, max):
        if include_icmp:
            # If ICMP data is included, the first 17 features are header features that require different normalization
            self.header_features = 17
        else:
            # Otherwise, the first 14 features are header features that require different normalization
            self.header_features = 14

        self.min = min[:self.header_features]
        self.max = max[:self.header_features]

    def __call__(self, sample):
        features = sample['item']
        payl_feats = features[self.header_features:]
        head_feats = features[:self.header_features]

        #mean = np.mean(head_feats)
        #std = np.std(head_feats)

        new_head_feats = (head_feats - self.min) / (self.max - self.min)
        new_payl_feats = (payl_feats / 255)
        return {'item': np.concatenate((new_head_feats, new_payl_feats), axis=0), 'label': sample['label']}


class BaseTrafficDataset(Dataset):
    """
    Dataset consisting of features generated from raw traffic data
    """

    def __init__(self, experiment, usage,
                 transform=transforms.Compose([ToTensor(), NormalizeTraffic(no_normalization=True)])):
        """

        :param config: MLFeaturesConfig
        :param transform:
        """
        #config = experiment.features_config

        dtype = experiment.dataset_interface.get_dtype()

        available_classes = []
        for c in experiment.dataset_interface.get_memmap_metadata(usage=usage).keys():
            if c not in experiment.exclude_classes:
                available_classes.append(c)


        # Access dataset via memory mapped system:
        self.memmaps = [] #List of tuples containing memory maps and corresponding labels
        self.indices = []

        # for label, metadata in config.memmap_metadata[usage].items():
        for label in available_classes:
            metadata = experiment.dataset_interface.get_memmap_metadata()[usage][label]
            # CrossEntropyLoss takes an index as input rather than a one-hot encoded vector!
            # https://discuss.pytorch.org/t/runtimeerror-multi-target-not-supported-newbie/10216/2
            # Therefore, use the "label integer = index" rather than a vector
            label = experiment.get_label_integer(label)

            # Skip classes without generated features
            if metadata[0] == '':
                continue

            # Impose the class constraint, if applicable
            if (usage != 'test') and (experiment.class_constraint >= 0):
                if metadata[1][0] > experiment.class_constraint:
                    metadata = list(metadata)
                    metadata[1][0] = experiment.class_constraint
                    metadata = tuple(metadata)
            self.memmaps.append(
                (
                    np.memmap(metadata[0], mode="r", shape=tuple(metadata[1]), dtype=dtype), label
                )
            )

            if len(self.indices) == 0:
                # metadata[1][0] = shape[0] = number of numpy arrays
                self.indices.append(metadata[1][0] - 1)
            else:
                self.indices.append(self.indices[-1] + metadata[1][0])

        self.num_classes = len(experiment.get_labels())
        self.transform = transform
        self.len = self.indices[-1] + 1
        self.dataset_interface = experiment.dataset_interface

    def __len__(self):
        return self.len

    def __getitem__(self, idx):
        for i in range(len(self.indices)):
            if idx <= self.indices[i]:
                if i == 0:
                    correct_idx = idx
                else:
                    correct_idx = idx - self.indices[i - 1] - 1

                sample = {'item': self.memmaps[i][0][correct_idx, :], 'label': self.memmaps[i][1]}
                if self.transform:
                    sample = self.transform(sample)

                return sample


class CompressedTrafficDataset(BaseTrafficDataset):

    def __getitem__(self, idx):
        for i in range(len(self.indices)):
            if idx <= self.indices[i]:
                if i == 0:
                    correct_idx = idx
                else:
                    correct_idx = idx - self.indices[i - 1] - 1

                sample = {'item': self.memmaps[i][0][correct_idx, :], 'label': self.memmaps[i][1]}

                # Before applying any transformation, apply decompression
                sample['item'] = self.dataset_interface.decompress_features(sample['item'])

                if self.transform:
                    sample = self.transform(sample)

                return sample