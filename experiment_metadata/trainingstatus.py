
class TrainingStatus:
    """
    Abstract class for inheritance, representing the training status of a machine learning algorithm.
    """
    def __init__(self, from_dict=None):
        if not (from_dict is None):
            self._from_dict(from_dict)
        else:
            self.dummy_value = 0.0

    def _from_dict(self, dictionary):
        """
        Read the necessary information from a dictionary to obtain all attributes of this class.

        :param dictionary: Dictionary containing all training status information
        :type dictionary: ``dict``
        :return: None
        """
        self.dummy_value = dictionary['dummy']

    def to_dict(self):
        """
        Get the training status as a dictionary that can be stored and used in a ``class:TrainingStatus`` at a later
        point in time.

        :return: The training status as stored in a dictionary
        :rtype: ``dict``
        """
        return {
            'dummy': self.dummy_value,
        }

    def get_current_lr(self):
        """
        Get the current learning rate of the training.

        :return: Current learning rate
        :rtype: ``float``
        """
        pass

    def get_current_avg_loss(self):
        """
        Get the current average loss of the training.

        :return: Current average loss
        :rtype: ``float``
        """
        pass

    def get_remaining_epochs(self):
        """
        Get the number of remaining training epochs

        :return: Number of remaining training epochs
        :rtype: ``int``
        """
        pass

    def get_current_accuracy(self):
        pass

    def is_continued(self):
        pass


class SupervisedTrainingStatus(TrainingStatus):
    """
    Class responsible for keeping track of the training progress of a supervised experiment. It allows for easy storage
    and loading of a model mid-training, in order to resume training a later point in time.

    :param epochs: Total number of training epochs for this training.
    :param lr: Initial learning rate of the training process.
    :param from_dict: Dictionary containing the training status as last saved.
    :type epochs: ``int``
    :type lr: ``float``
    :type from_dict: ``dict``
    """
    def __init__(self, epochs=None, lr=None, from_dict=None):
        """
        Constructor for the TrainingStatus class. An object of this class can be constructed in two different
        situations:
        * The training starts from scratch. In this case, the number of training epochs and the initial learning rate
        must be provided.
        * The training resumes from where it previously left off: Only from_dict is required.

        :param epochs: Total number of training epochs for this training.
        :param lr: Initial learning rate of the training process.
        :param from_dict: Dictionary containing the training status as last saved.
        :type epochs: ``int``
        :type lr: ``float``
        :type from_dict: ``dict``
        """
        if not (from_dict is None):
            self._from_dict(from_dict)
        else:
            self.remaining_epochs = epochs
            self.current_lr = lr
            self.current_accuracy = 0.0
            self.current_avg_loss = float('inf')
            self.continued = False

    def to_dict(self):
        """
        Get the training status as a dictionary that can be stored and used in a ``class:TrainingStatus`` at a later
        point in time.

        :return: The training status as stored in a dictionary
        :rtype: ``dict``
        """
        return {
            'rem_epochs': self.remaining_epochs,
            'current_lr': self.current_lr,
            'current_acc': self.current_accuracy,
            'current_loss': self.current_avg_loss,
        }

    def update(self, same_epoch=False, accuracy=None, avg_loss=None, lr=None):
        """
        Update the training status after a training step and optional validation step.
        :param same_epoch: If True, the current does not need to be changed.
        :param accuracy: The validation accuracy after the latest training epoch, optional.
        :param avg_loss: The average validation loss after the latest training epoch, optional.
        :param lr: The learning rate after the latest training epoch, optional.
        :type same_epoch: ``bool``
        :type accuracy: ``float``
        :type avg_loss: ``float``
        :type lr: ``float``
        :return: None
        """
        if not same_epoch:
            self.remaining_epochs -= 1
        if not (avg_loss is None):
            self.current_avg_loss = avg_loss
        if not (accuracy is None):
            self.current_accuracy = accuracy
        if not (lr is None):
            self.current_lr = lr

    def is_complete(self):
        """
        Check if the training has completed.

        :return: True if the training has completed.
        :rtype: ``bool``
        """
        return self.remaining_epochs <= 0

    def _from_dict(self, dictionary):
        """
        Read the necessary information from a dictionary to obtain all attributes of this class.
        :param dictionary:
        :return:
        """
        self.remaining_epochs = dictionary['rem_epochs']
        self.current_lr = dictionary['current_lr']
        self.current_accuracy = dictionary['current_acc']
        self.current_avg_loss = dictionary['current_loss']
        self.continued = True

    def get_current_lr(self):
        """
        Get the current learning rate of the training.

        :return: Current learning rate
        :rtype: ``float``
        """
        return self.current_lr

    def get_current_accuracy(self):
        """
        Get the current learning rate of the training.

        :return: Current learning rate
        :rtype: ``float``
        """
        return self.current_accuracy

    def get_current_avg_loss(self):
        """
        Get the current average loss of the training.

        :return: Current average loss
        :rtype: ``float``
        """
        return self.current_avg_loss

    def get_remaining_epochs(self):
        """
        Get the number of remaining training epochs

        :return: Number of remaining training epochs
        :rtype: ``int``
        """
        return self.remaining_epochs

    def add_epochs(self, epochs):
        """
        Increase the number of remaining epochs for this training.

        :param epochs: Number of epochs to increase the remaining epochs with.
        :type epochs: ``int``
        :return: None
        """
        self.remaining_epochs += epochs

    def is_continued(self):
        return self.continued


class SDAUnsupervisedTrainStatus:
    """
        Class responsible for keeping track of the unsupervised training progress of an Stacked Denoising Autoencoder.
        It allows for easy storage and loading of a model mid-training, in order to resume training a
        later point in time.

        :param from_dict: Dictionary containing the training status as last saved.
        :type from_dict: ``dict``
        """

    def __init__(self, from_dict=None, **kwargs):
        """
        Constructor for the TrainingStatus class. An object of this class can be constructed in two different
        situations:
        * The training starts from scratch. In this case, some required information should be provided in the kwargs
        * The training resumes from where it previously left off: Only from_dict is required.

        :param from_dict: Dictionary containing the training status as last saved.
        :type from_dict: ``dict``
        """
        # Train the autoencoder
        # parameter nodig: lossverandering waarbij training stopt

        # Ook bijhouden:
        # aantal getrainde layers
        # progress in laatst getrainde layer
        # klaar met unsupervised training
        if not (from_dict is None):
            self._from_dict(from_dict)
        else:
            # Status for the AE stack
            self.n_ae = kwargs['n_ae']  # Number of AutoEncoders to train
            self.current_ae = 0         # Index of the AE currently training

            self.overview = {}

            epochs = kwargs['n_epochs']
            lr = kwargs['lr']
            threshold = kwargs.get('lr_threshold', 0.001)
            scheduler_patience = kwargs.get('scheduler_patience', 5)

            for i in range(self.n_ae):
                # Status for the current layer
                self.overview[str(i)] = {
                    'rem_epochs': epochs,
                    'current_lr': lr,
                    'threshold': threshold,
                    'patience': scheduler_patience,
                    'current_avg_loss': float('inf'),
                }

            self.continued = False

    def to_dict(self):
        """
        Get the training status as a dictionary that can be stored and used in a ``class:TrainingStatus`` at a later
        point in time.

        :return: The training status as stored in a dictionary
        :rtype: ``dict``
        """
        return {
            'n_ae': self.n_ae,
            'current_ae': self.current_ae,
            'overview': self.overview,
        }

    def update(self, same_epoch=False, avg_loss=None, lr=None):
        """
        Update the training status after a training step and optional validation step for the current AE.

        :param same_epoch: If True, the current epoch does not need to be changed. Default false
        :param avg_loss: The average validation loss after the latest training epoch, optional.
        :param lr: The learning rate after the latest training epoch, optional.
        :type same_epoch: ``bool``
        :type avg_loss: ``float``
        :type lr: ``float``
        :return: None
        """
        if not same_epoch:
            self.overview[str(self.current_ae)]['rem_epochs'] -= 1
        if not (avg_loss is None):
            self.overview[str(self.current_ae)]['current_avg_loss'] = avg_loss
        if not (lr is None):
            self.overview[str(self.current_ae)]['current_lr'] = lr

    def next_ae(self):
        """
        Set the current AE to the next AE, if applicable
        :return:
        """
        if (self.current_ae + 1) < self.n_ae:
            self.current_ae += 1

    def set_ae(self, ae):
        if ae < self.n_ae:
            self.current_ae = ae

    def is_complete(self):
        """
        Check if the training has completed.

        :return: True if the training has completed.
        :rtype: ``bool``
        """
        return self.overview[str(self.n_ae - 1)]['rem_epochs'] <= 0

    def _from_dict(self, dictionary):
        """
        Read the necessary information from a dictionary to obtain all attributes of this class.
        :param dictionary:
        :return:
        """
        self.n_ae = dictionary['n_ae']
        self.current_ae = dictionary['current_ae']
        self.overview = dictionary['overview']
        self.continued = True

    def get_current_lr(self):
        """
        Get the current learning rate of the training.

        :return: Current learning rate
        :rtype: ``float``
        """
        return self.overview[str(self.current_ae)]['current_lr']

    def get_current_avg_loss(self):
        """
        Get the current average loss of the training.

        :return: Current average loss
        :rtype: ``float``
        """
        return self.overview[str(self.current_ae)]['current_avg_loss']

    def get_remaining_epochs(self):
        """
        Get the number of remaining training epochs

        :return: Number of remaining training epochs
        :rtype: ``int``
        """
        return self.overview[str(self.current_ae)]['rem_epochs']

    def add_epochs(self, epochs):
        """
        Increase the number of remaining epochs for the training of the current ae.

        :param epochs: Number of epochs to increase the remaining epochs with.
        :type epochs: ``int``
        :return: None
        """
        self.overview[str(self.current_ae)]['rem_epochs'] += epochs

    def is_continued(self):
        return self.continued
