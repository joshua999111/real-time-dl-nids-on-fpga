import os
import json

import numpy as np

from experiment_metadata.trainingstatus import TrainingStatus, SupervisedTrainingStatus
from experiment_metadata.optimizerconfig import get_optimizer
from experiment_metadata.config import Config, configuration_exists
from experiment_metadata.enums import Optimizer

from dataset_interface.dataset_interface import DatasetInterface


def load_experiment(config_file, supervised=True):
    """
    Load Experiment from .json config file

    :param config_file:
    :return:
    """
    with open(config_file, 'r') as f:
        config = json.load(f)

    if supervised:
        return SupervisedMLExperiment(root=config.root, name=config.name)

def remove_machine_learning_experiment(name, root, verbose=True):
    """
    Remove the configuration of a model for machine learning.
    :param name:
    :param root:
    :param verbose:
    :return:
    """
    config_file = os.path.join(root, "models_config", name + ".json")
    model_root = os.path.join(root, 'models', name)

    if verbose:
        print("Removing model configuration:", config_file)
    # Remove the configuration file
    os.remove(config_file)

    if verbose:
        print("Removing model:", model_root + '.model')
    # Remove the model
    if os.path.isfile(model_root):
        os.remove(model_root)


class Experiment(Config):
    def __init__(self, root, name, dataset_interface=None, model_name=None, use_existing=True, **kwargs):
        """
        Constructor for Experiment.

        :param root: Path to root directory of machine learning data.
        :param name: Name of the experiment.
        :param ml_features_config: Configuration of the machine learning features.
        :param model_name: Name of the machine learning model to use.
        :param use_existing: If True and applicable, use the configuration of an existing machine learning experiment.
        :type root: ``str``
        :type name: ``str``
        :type ml_features_config: ``MLFeaturesConfig``
        :type model_name: ``enums.Model``
        :param \**kwargs: Arbitrary keyword arguments, given below.

        :Keyword Arguments:
            * **class_constraint** (``int``) --
              Maximum number of elements per class that will be included in the training. Does not change validation or
              testing.
            * **exclude_classes** (``list``) --
              List of classes that will be excluded from the machine learning.
            * **lr** (``float``) --
              Initial learning rate.
            * **momentum** (``float``) --
              Initial momentum.
            * **n_epochs** (``int``) --
              Number of training epochs.
            * **continue_tr** (``bool``) --
              If True, the training will be saved after each epoch. Training can later be resumed from the last saved
              epoch, or additional training epochs can be added.
            * **label_mapping** (``dict`` of ``{````str``:```str``, ...``}``) --
              Mapping that can be used to map class labels. The ``dict``contains pairs of label names. key-values will
              be mapped to the item-values. This can be used to map separate classes to the same label.
        """

        super().__init__(root, name)
        #exists = os.path.isfile(self.config_root)
        exists = configuration_exists(self.name, self.root)

        if use_existing and exists:
            self._get_existing_config()
            return
        else:
            if model_name is None:
                raise ValueError("No machine learning model specified for new experiment.")
            if dataset_interface is None:
                raise ValueError("No interface to the dataset provided.")
            # If a configuration actually exists, it will not be used (otherwise _get_existing_config would have been
            # called). Therefore remove the existing configuration.
            if exists:
                remove_machine_learning_experiment(self.name, self.root, verbose=True)

        self.model = model_name.name
        self.dataset_interface = dataset_interface
        self._initial_configuration(**kwargs)

    def _update_from_existing_config_baseline(self, config):
        # General
        self.model = config['model']
        self.dataset_interface = DatasetInterface(root_directory=config['dataset_interface'])

        # Hyperparameters
        self.lr = config['hyper']['lr']

        self.n_epochs = config['hyper']['n_epochs']
        self.continue_tr = config['hyper']['continue_tr']
        self.optimizer = get_optimizer(from_dict=config['hyper']['optim'])
        self.batch_size = config['hyper']['batch_size']
        self.train_steps_per_validation = config['hyper']['train_steps_per_validation']
        self.num_workers = config['hyper']['num_workers']

        # Stopping condition
        self.stopping_condition = config['stopping_condition']

        # Storage:
        self.model_path = config['storage']['model_path']

        # labels:
        self.label_mapping = config['label_mapping']

        # Best
        self.best_model_path = config['best']['model']
        self.best_loss = config['best']['loss']

        self.class_constraint = config['class_constraint']
        self.exclude_classes = config['exclude_classes']

    def _update_from_existing_config(self, config):
        """
        Update the data members of the object by reading their correct state
        from the stored configuration.

        Params:
        config(dict): Stored configuration dictionary
        """

        self._update_from_existing_config_baseline(config)

        self.status = \
            {
                'train':
                    {
                        'status': TrainingStatus(from_dict=config['status']['train']['status']),
                        'complete': config['status']['train']['complete']
                    },
                'test': config['status']['test']
            }

    def _initial_configuration_baseline(self, **kwargs):
        self.class_constraint = kwargs.get('class_constraint', 100000)
        self.exclude_classes = kwargs.get('exclude_classes', [])

        # Hyperparameters
        self.lr = kwargs.get('lr', 0.001)  # Initial learning rate
        self.n_epochs = kwargs.get('n_epochs', 2)  # Initial number of epochs
        self.continue_tr = kwargs.get('continue_tr', True)  # Continue training after interruption

        self.optimizer = get_optimizer(kwargs.get('optimizer', Optimizer.SGD), from_dict=None, **kwargs)
        self.batch_size = kwargs.get('batch_size', 256)
        self.train_steps_per_validation = kwargs.get('train_steps_per_validation', -1)
        self.num_workers = kwargs.get('num_workers', 1)

        # Stopping condition
        self.stopping_condition = kwargs.get('stopping_condition', 'epochs')

        # Storage:
        self.model_path = os.path.join(self.root, "models", self.name + ".model")

        # labels:
        self.label_mapping = kwargs.get('label_mapping', {})

        # Best results
        self.best_model_path = os.path.join(self.root, "models", self.name + "-best.model")
        self.best_loss = float('inf')

    def _initial_configuration(self, **kwargs):
        self._initial_configuration_baseline(**kwargs)

        # Status
        self.status = {
            'train': {
                'status': TrainingStatus(),
                'complete': False,
            },
            'test': {
                'complete': False,
                'accuracy': 0.0,
            },
        }

        # Store the current, initialized configuration
        self._store_configuration()

    def _get_config_dict(self):
        return {
            'config_root': self.config_root,
            'name': self.name,
            'root': self.root,
            'model': self.model,
            'dataset_interface': self.dataset_interface.root_directory,
            'status': {
                'train': {
                    'status': self.status['train']['status'].to_dict(),
                    'complete': self.status['train']['complete'],
                },
                'test': self.status['test'],
            },
            'hyper': {
                'lr': self.lr,
                'optim': self.optimizer.to_dict(),
                'n_epochs': self.n_epochs,
                'train_steps_per_validation': self.train_steps_per_validation,
                'batch_size': self.batch_size,
                'continue_tr': self.continue_tr,
                'num_workers': self.num_workers,
            },
            'storage': {
                'model_path': self.model_path,
            },
            'best': {
                'model': self.best_model_path,
                'loss': self.best_loss,
            },
            'label_mapping': self.label_mapping,
            'class_constraint': self.class_constraint,
            'exclude_classes': self.exclude_classes,
            'stopping_condition': self.stopping_condition,
        }

    def map_label(self, label):
        """
        Apply the label mapping of this experiment to the provided label. If no mapping exists, the label remains
        unchanged.
        :param label: Input label to be mapped by the label map as specified for this experiment.
        :type label: ``str``
        :return: The mapped label if applicable, or the unchanged label otherwise.
        :rtype: ``str``
        """
        if label in self.label_mapping.keys():
            return self.label_mapping[label]
        else:
            return label

    def get_labels(self):
        """
        Get the labels of all classes considered in this experiment, taking into account any label mapping that could be
        applied.

        :return: A list of the labels after mapping of all classes.
        :rtype: ``list`` of [``str``]
        """
        labels = []
        for label in self.dataset_interface.get_labels():
            if label not in self.exclude_classes:
                mapped_label = self.map_label(label)
                if mapped_label not in labels:
                    labels.append(mapped_label)
        return labels

    def get_label_integer(self, label):
        """
        Get the index corresponding to a specific label. Will raise an exception if the label does not comprise a
        class in the experiment.

        :param label: Label to get index for.
        :type label: ``str``
        :return: The index corresponding to the provided label.
        :rtype: ``str``
        """
        label = self.map_label(label)
        labels = self.get_labels()
        for i in range(len(labels)):
            if label == labels[i]:
                return i
        raise ValueError('Provided label {} not recognized!'.format(label))

    def get_results_cm(self):
        """
        Get the experimental results, if any, as a confusion-matrix dictionary.

        :return: Confusion matrix in a dictionary if available, None otherwise
        :rtype: ``dict`` or None
        """
        path = os.path.join(self.root, 'results', 'cm', self.name + '.json')
        if os.path.isfile(path):
            with open(path, 'r') as f:
                return json.load(f)
        return None

    def store_results_cm(self, cm):
        """
        Store the provided results confusion matrix in a single file at its assigned location.

        :param cm: Dictionary containing the results report.
        :type cm: ``dict``
        :return: None
        """
        path = os.path.join(self.root, 'results', 'cm', self.name + '.json')
        with open(path, 'w') as f:
            json.dump(cm, f)

    def store_results_text(self, report):
        """
        Store the provided results report as a text file at its assigned location.

        :param report: String containing the results report.
        :type report: ``str``
        :return: None
        """
        path = os.path.join(self.root, 'results', 'text', self.name + '.txt')
        with open(path, 'w') as f:
            f.write(report)

    def store_results_json(self, report):
        """
        Store the provided results report as a json file at its assigned location.

        :param report: Dictionary containing the results report.
        :type report: ``dict``
        :return: None
        """
        path = os.path.join(self.root, 'results', 'json', self.name + '.json')
        with open(path, "w") as f:
            json.dump(report, f)

    def get_current_average_loss(self):
        """
        Get the current average validation loss in the training process.

        :return: The current average validation loss in the training process.
        :rtype: ``float``
        """
        return self.status['train']['status'].get_current_avg_loss()

    def get_remaining_epochs(self):
        """
        Get the number of remaining training epochs. If called during a training epoch, the current epoch is included.
        :return: The number of remaining training epochs.
        :rtype: ``int``
        """
        return self.status['train']['status'].get_remaining_epochs()

    def get_current_epoch(self):
        return self.n_epochs - self.get_remaining_epochs()

    def get_current_lr(self):
        """
        Get the current learning rate in the training process.

        :return: The current learning rate in the training process.
        :rtype: ``float``
        """
        return self.status['train']['status'].get_current_lr()

    def get_current_accuracy(self):
        """
        Get the current validation accuracy in the training process.

        :return: The current validation accuracy in the training process.
        :rtype: ``float``
        """
        return self.status['train']['status'].get_current_accuracy()

    def is_continued(self):
        """
        Check if the experiment is resuming a previously interrupted/finished training.

        :return: True if the experiment is resuming training.
        """
        return self.status['train']['status'].is_continued()

    def export_validation_samples(self, n_samples=10):
        """
        Export validation samples relevant for this experiment.

        :param n_samples: Number of samples to include per class
        :type n_samples: ``int``

        :return:
        """
        samples_list = []
        label_int_lists = []

        # Extract data
        for label in self.get_labels():

            # Check if any label mapping has taken place:
            original_labels = []
            for original_label, mapped_label in self.label_mapping.items():
                # If the label was mapped
                if mapped_label == label:
                    # Add the original label
                    original_labels.append(original_label)

            # If there was no mapping, label is equal to the original label, which we need
            if len(original_labels) == 0:
                original_labels.append(label)

            # Now request samples for the original labels, spread as evenly as possible
            current_samples_list = []
            current_og_label = 0
            while len(current_samples_list) < n_samples:

                # Add a random sample to the list, for the current label
                # We will cycle between all origial labels
                # For example, if the three Web Attacks in CICIDS2017 were mapped to the label "Web Attacks",
                # We would take one sample at a time for each different Web Attack in turn
                current_samples_list.append(self.dataset_interface.get_sample(
                    label=original_labels[current_og_label],
                    usage='valid',
                    seed=False))

                current_og_label = (current_og_label + 1) % len(original_labels)

            samples_list += current_samples_list
            label_int_lists += [self.get_label_integer(label) for _ in range(n_samples)]

        data_array = np.array(samples_list)
        eval_array = np.array(label_int_lists)

        return data_array, eval_array


class SupervisedMLExperiment(Experiment):

    def _initial_configuration(self, **kwargs):
        self._initial_configuration_baseline(**kwargs)

        # Status
        self.status = {
            'train': {
                'status': SupervisedTrainingStatus(epochs=self.n_epochs, lr=self.lr),
                'complete': False,
            },
            'test': {
                'complete': False,
                'accuracy': 0.0,
            },
        }

        # Scheduler settings
        self.patience = kwargs.get('patience', 10)
        self.threshold = kwargs.get('threshold', 0.001)
        self.cooldown = kwargs.get('cooldown', 0)
        self.factor = kwargs.get('factor', 0.1)

        # Store the current, initialized configuration
        self._store_configuration()

    def _get_config_dict(self):
        config = super()._get_config_dict()

        config['scheduler'] = {
            'patience': self.patience,
            'threshold': self.threshold,
            'cooldown': self.cooldown,
            'factor': self.factor,
        }

        return config

    def _update_from_existing_config(self, config):
        self._update_from_existing_config_baseline(config)

        self.status = \
            {
                'train':
                    {
                        'status': SupervisedTrainingStatus(from_dict=config['status']['train']['status']),
                        'complete': config['status']['train']['complete']
                    },
                'test': config['status']['test']
            }

        self.patience = config['scheduler']['patience']
        self.threshold = config['scheduler']['threshold']
        self.cooldown = config['scheduler']['cooldown']
        self.factor = config['scheduler']['factor']

    def update_train_status(self, same_epoch=False, **kwargs):
        """
        Update the training status. While all keyword arguments are optional, they should be utilized if the training is
        to be interrupted/extended/resumed.

        :param same_epoch: If True, the current does not need to be changed.
        :type same_epoch: ``bool``
        :Keyword Arguments:
            * **accuracy** (``float``) --
              The validation accuracy after the latest training epoch, optional.
            * **avg_loss** (``float``) --
              The average validation loss after the latest training epoch, optional.
            * **lr** (``float``) --
              The learning rate after the latest training epoch, optional.
        :return: None
        """
        self.status['train']['status'].update(same_epoch=same_epoch, **kwargs)
        self.status['train']['complete'] = self.status['train']['status'].is_complete()

    def add_training_epochs(self, epochs):
        """
        Increase number of required training epochs to keep training.

        :param epochs: Number of epochs to add to the total number of training epochs
        :type epochs: ``int``
        :return: None
        """
        self.n_epochs += epochs
        self.status['train']['status'].add_epochs(epochs)
        self.status['train']['complete'] = False