import argparse
import os
import sys

import numpy as np

from dataset_interface.help_functions import mkdir
from experiment_metadata.enums import Model, is_supervised, Optimizer
from experimentation import experimentation, add_training_epochs, get_existing_experiment, conduct_inference, \
    conduct_sanity_check
from quantization.brevitas.export import export_to_finn_onnx


def setup_prelim(root):
    print(root)

    config = os.path.join(root, "config")
    models = os.path.join(root, "models")
    memmaps = os.path.join(root, "memmaps")
    results = os.path.join(root, "results")
    results_text = os.path.join(results, 'text')
    results_json = os.path.join(results, 'json')
    results_cm = os.path.join(results, 'cm')

    mkdir(config)
    mkdir(memmaps)
    mkdir(models)
    mkdir(results)
    mkdir(results_text)
    mkdir(results_json)
    mkdir(results_cm)

#from dataset_config import get_config_from_file
#from ml_config import get_ml_experiment, get_ml_features_config
#from learning.learning import get_number_parameters, get_flops
#from help_functions import format_storage_size, num_parameters_to_storage_size, format_macs
#from data_exporter import export_relevant_data, change_configuration_internal_root


parser = argparse.ArgumentParser(description='Conduct machine learning operations')
subparsers = parser.add_subparsers(help='Machine learning operation', dest='subcommand')
parser.add_argument('--root', required=False, default='')
learning_parser = subparsers.add_parser('learning')
results_parser = subparsers.add_parser('results')
export_parser = subparsers.add_parser('export')
inference_parser = subparsers.add_parser('inference')
prolong_training_parser = subparsers.add_parser('add_training_epochs')

# Training parser
learning_parser.add_argument('-d', '--dataset', dest='dataset_dir', help='Preprocessed dataset directory path')
learning_parser.add_argument('-e', '--experiment', '-n', '--name',
                             dest='name', help='Name of the experiment', required=True)
learning_parser.add_argument('--use_existing', dest='use_existing', default=True, help='If True, continue existing '
                                                                                       'experiments if applicable.')

# Required args:
# Model
learning_parser.add_argument('--model', dest='model', help='Desired architecture', required=True)

# Optional args:
# Learning rate
learning_parser.add_argument('--lr', dest='lr', help='Desired learning rate', type=float)
# Number of epochs
learning_parser.add_argument('--n_epochs', dest='n_epochs', default=10, help='(Maximum) number of training epochs',
                             type=int)
# Excluded classes
learning_parser.add_argument('--exclude', dest='exclude_classes', nargs='*', default=[],
                             help='Classes that will be excluded from training')
# Class constraint
learning_parser.add_argument('--class_constraint', dest='class_constraint', default=-1, type=int,
                             help='Maximum number of training/validation samples per class')
# Label mapping
learning_parser.add_argument('--label_mapping', dest='label_mapping_list', default=[], nargs='*',
                             help='Label mapping [ORIGINAL LABEL] [MAPPED LABEL] [ORIGINAL LABEL] [MAPPED LABEL] ...')
# Optimizer
learning_parser.add_argument('--optimizer', dest='optimizer', default='SGD', help='Desired optimizer')
# Stopping condition
learning_parser.add_argument('--stopping_condition', dest='stopping_condition', default='epochs',
                             help='Stopping condidation for the training process', choices={'loss_decrease', 'epochs'})
# Number of workers
learning_parser.add_argument('--num_workers', dest='num_workers', default=8,
                             help='Number of multithreaded workers')

# Minibatch size
learning_parser.add_argument('--batch_size', dest='batch_size', default=256,
                             help='Number of multithreaded workers')

# Continue training
learning_parser.add_argument('--continue_training', dest='continue_tr', type=bool,
                             help='Continue previous training')


# Functionality to add training epochs
prolong_training_parser.add_argument('-e', '--experiment', '-n', '--name',
                                     dest='name', help='Name of the experiment', required=True)

prolong_training_parser.add_argument('--n_epochs', dest='n_epochs', help='Number of epochs to add to training',
                                     required=True, type=int)

# Functionality to export experiments to a specific representation
export_parser.add_argument('--representation', dest='rep', help='Either onnx or valid_samples')
export_parser.add_argument('--name', dest='experiment_name')
export_parser.add_argument('--destination', dest='output_path')
export_parser.add_argument('--n_samples', dest='n_samples', help='When exporting validation data, this is the number '
                                                                 'of samples per class to export', type=int)
# Functionality to conduct inference
inference_parser.add_argument('--name', dest='experiment_name')
inference_parser.add_argument('--destination', dest='output_path')
inference_subparsers = inference_parser.add_subparsers(help='Machine learning operation', dest='inference_type')

supervised_inference = inference_subparsers.add_parser('supervised')
supervised_inference.add_argument('--data_file', dest='data_file', type=str)
supervised_inference.add_argument('--eval_file', dest='eval_file', type=str)
supervised_inference.add_argument('--file_type', dest='file_type', type=str)

unsupervised_inference = inference_subparsers.add_parser('unsupervised')
supervised_inference.add_argument('--data', dest='data_file')
supervised_inference.add_argument('--type', dest='file_type')

sanity_check_inference = inference_subparsers.add_parser('sanity_check')

# Parse all provided arguments
p = parser.parse_args()

#p = parser.parse_args(['--root', '/home/laurens/Experiment/Experiments/', 'learning',
#                       '--dataset', '/media/laurens/Preprocessed datasets/Datasets/CICIDS2017_base_PCCN_hepa/',
#                       '--name', 'CICIDS2017_PCCN_HEPA', '--model', 'PCCNN', '--n_epochs', '10', '--label_mapping',
#                       "Web Attack Brute Force", "Web Attack", "Web Attack Sql Injection", "Web Attack",
#                       "Web Attwack XSS", "Web Attack", '--continue_tr', 'True'])
if p.root == '':
    root = ''   # Default working root

else:
    root = p.root

if not os.path.isdir(root):
    print('Invalid experiments root directory')
    print('Root directory provided was {}'.format(root))
    sys.exit(0)
else:
    setup_prelim(root)

if p.subcommand == 'learning':
    print('Interpreting training instructions')

    # Extract label mapping
    label_mapping = {}
    for i in range(0, len(p.label_mapping_list), 2):
        label_mapping[p.label_mapping_list[i]] = p.label_mapping_list[i+1]

    kwargs = {
        'model_name': Model[p.model],
        #'model_name': p.model,
        'exclude_classes': p.exclude_classes,
        'n_epochs': p.n_epochs,
        'class_constraint': p.class_constraint,
        'label_mapping': label_mapping,
        'optimizer': Optimizer[p.optimizer],
        'stopping_condition': p.stopping_condition,
        'num_workers': p.num_workers,
        'batch_size': p.batch_size,
        'dataset_dir': p.dataset_dir,
    }

    print('RECEIVED MODEL NAME:', kwargs['model_name'])

    if p.lr:
        kwargs['lr'] = p.lr
    if p.continue_tr:
        kwargs['continue_tr'] = p.continue_tr

    experimentation(name=p.name, root=root,
                    use_existing=p.use_existing,
                    supervised=is_supervised(kwargs['model_name']),
                    **kwargs)
elif p.subcommand == 'add_training_epochs':
    add_training_epochs(p.name, root, p.n_epochs)

elif p.subcommand == 'export':

    print('Loading experiment {} at root {}'.format(p.experiment_name, root))
    experiment = get_existing_experiment(p.experiment_name, root=root)
    rep = p.rep
    output_path = p.output_path

    if rep == 'onnx':
        export_to_finn_onnx(experiment, output_path=output_path)
    elif rep == 'valid_samples':
        # Export a batch of validation samples:
        # These will be provided in two files, '<name>-data.npy' and '<name>-eval.npy'
        # The '-data.npy' file contains dataset samples, and '-eval.npy' contains the corresponding output index
        # Shapes will be (n_samples, *data_sample_shape) and (n_samples,)
        n_samples = p.n_samples

        data_array, eval_array = experiment.export_validation_samples(n_samples=n_samples)

        # Save data:
        data_filename = experiment.name + '-data.npy'
        eval_filename = experiment.name + '-eval.npy'

        np.save(os.path.join(output_path, data_filename), data_array)
        np.save(os.path.join(output_path, eval_filename), eval_array)

        print('Exported {} data samples to {} and {}'.format(n_samples,
                                                             os.path.join(output_path, data_filename),
                                                             os.path.join(output_path, eval_filename)))

    else:
        print('Specified representation \'{}\' is not supported.'.format(rep))

elif p.subcommand == 'inference':
    # Conduct inference with the trained model
    print('Loading experiment {} at root {}'.format(p.experiment_name, root))
    experiment = get_existing_experiment(p.experiment_name, root=root)

    # Output path to print the results to
    output_path = p.output_path

    # Input rep: The method to provide input data
    inference_type = p.inference_type

    if inference_type == 'supervised':
        data_file = p.data_file
        eval_file = p.eval_file
        file_type = p.file_type

        conduct_inference(experiment, data_file=data_file, evaluation_file=eval_file,
                          file_type=file_type, output_path=output_path, supervised=True)

    elif inference_type == 'unsupervised':
        data_file = p.data_file
        file_type = p.file_type

        conduct_inference(experiment, data_file=data_file, evaluation_file=None, file_type=file_type,
                          output_path=output_path, supervised=True)

    elif inference_type == 'sanity_check':
        conduct_sanity_check(experiment=experiment, output_path=output_path)

    else:
        print('Specified inference type \'{}\' is not supported.'.format(inference_type))

elif p.subcommand == 'results':
    print('Doing something with results')
