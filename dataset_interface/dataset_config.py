"""
This module specifies the classes that are used to process and store the configuration of different datasets. Currently, the
supported datasets are: ISCX2012, UNSW-NB15 and CICIDS2017. The configuration classes, respectively :class:`ISCX2012Config`,
:class:`UNSW_NB15Config` and :class:`CICIDS2017Config` inherit from the base class :class:`DatasetConfig`.

The configuration objects can be used dynamically throughout the processing of a dataset, but needs to be stored in order to
remain useful. It is stored in the `config` subdirectory (`self.config_root`) of the directory containing all datasets (referred to as `root`),
named after the dataset it represents. The datasets themselves, as well as their processed forms, are stored in the subdirectory named after them
(`self.data_root`).
"""

import os
from .enums import Dataset, LinkLayer
from .config import configuration_exists, Config
from .exceptions import DatasetConfigurationConstructionException, DatasetPreprocessInvalidArgument
from . import help_functions as hf
import json


def remove_configuration(name, root, verbose=False):
    """
    Remove the dataset at the specified location, as well as its configuration. Note that this function does **NOT** check whether
    the configuration or the dataset actually exist.

    :arg dataset: The dataset for which a configuration is searched
    :arg root: The root directory containing all datasets.
    :arg verbose: If True, the function can output additional printed information.
    :type dataset: `enums.Dataset` object specifying the dataset
    :type root: ``str``
    :type verbose: ``bool``
    """
    config_file = os.path.join(root, "config", name + ".json")
    dataset_root = os.path.join(root, name)

    # Remove the configuration file
    os.remove(config_file)

    # Remove the directory containing all datasets information (PCAPs, CSVs, sorted flows,...)
    hf.remove_dir(dataset_root, True, verbose)


def get_config_from_file(config_file, export_mode=False):
    """
    Get the corresponding dataset configuration from a JSON dataset configuration storage file.
    :param config_file: Path to the dataset configuration files (/.../dataset.json)
    :type config_file: ``str``
    :param export_mode: If True, operate in export mode
    :type export_mode: ``bool``
    :return: The dataset configuration object
    :rtype: Subclass of :class:`DatasetConfig`
    """

    if export_mode:
        export = config_file
    else:
        export = None

    with open(config_file, 'r') as f:
        data_dict = json.load(f)
        dataset = Dataset[data_dict['dataset']]
        root = data_dict['root']
        name = data_dict['name']

    if dataset == Dataset.ISCX2012:
        return ISCX2012Config(root, name=name, export_mode=export)
    elif dataset == Dataset.UNSW_NB15:
        return UNSW_NB15Config(root, name=name, export_mode=export)
    elif dataset == Dataset.CICIDS2017:
        return CICIDS2017Config(root, name=name, export_mode=export)


def change_configuration_root(config_file, new_root):
    """
    Change the configuration root of the dataset inside the configuration file.
    NOTE: THIS DOES NOT CHANGE DATASET-SPECIFIC PATH INFORMATION SUCH AS THE XML-ROOT!

    :param config_file: Path to the configuration file to be edited
    :param new_root: New root that needs to be used inside the configuration file
    :return: None
    """
    with open(config_file, 'r') as f:
        data_dict = json.load(f)

    data_dict['root'] = os.path.join(new_root, 'datasets')
    data_dict['storage']['data_root'] = os.path.join(data_dict['root'], data_dict['name'])
    data_dict['storage']['config_root'] = os.path.join(data_dict['root'], 'config', data_dict['name'] + '.json')

    with open(config_file, 'w') as f:
        json.dump(data_dict, f)


def get_dataset_days(dataset):
    """
    Get a list with the dataset days that are used in the specified dataset.

    :param dataset: Specified dataset
    :type dataset: ``Dataset``
    :return: List with all dataset days
    :rtype: ``list`` of ``str``
    """
    if dataset == Dataset.ISCX2012:
        return ["11-friday", "12-saturday", "13-sunday", "14-monday", "15-tuesday", "16-wednesday", "17-thursday"]
    elif dataset == Dataset.UNSW_NB15:
        return ["22-01", "17-02"]
    elif dataset == Dataset.CICIDS2017:
        return ["monday", "tuesday", "wednesday", "thursday", "friday"]


class DatasetConfig(Config):
    """
    :class:`DatasetConfig` implements the base class for the configuration of a dataset. It can be inherited from to add
    aditional functionality, as each dataset is different in some regards.

    :arg dataset: The dataset for which a configuration is searched
    :arg root: The root directory containing all datasets.
    :arg use_existing: If True, use an existing form of the dataset if possible. Otherwise, erase any existing data \
    for this dataset and start from scratch.
    :arg days: List containing all days featured in the dataset.
    :arg verbose: If True, the function can output additional printed information.
    :type dataset: `enums.Dataset` object specifying the dataset
    :type root: ``str``
    :type use_existing: ``bool``
    :type days: ``list`` of ``str``
    :type verbose: ``bool``

    :param \**kwargs: Arbitrary keyword arguments, given below.

    :Keyword Arguments:
        * **use_c_libs** (``bool``) --
          If True, use optimized C libraries. **CURRENTLY DEPRECATED**, set False.
        * **fast_sort** (``bool``) --
          If True, immediately sort extracted packets and store them in a flow tree rather than just printing them to a file.
        * **bidirectional** (``bool``) --
          If True, label not only packets going from source to destination, but also the return packets going from that destination to the source in the same flow.
        * **chronological** (``bool``) --
          If True, label and store both leaving as well returning packets from one flow chronologicall in one flow.
        * **include_ts** (``bool``) --
          If True, include timestamps in the extraction of packets as well as the sorting and labelling.
        * **batch_size** (``int``) --
          Number of packets that will be processed at once during their extraction and sorting.
        * **max_ram_usage** (``int``) --
          Maximum amount of RAM the program may consume, in bytes.
        * **avg_packet_size** (``bool``) --
          The average size of packets in a flow (in characters).
        * **clear_pcaps** (``bool``) --
          If True, remove pcaps from storage after they have been used.
        * **clear_csvs** (``bool``) --
          If True, remove label csv files from storage after they have been used.
        * **clear_raw_packets** (``bool``) --
          If True, remove the extracted raw (unsorted) packets after they have been used to create sorted flows.
        * **clear_sorted_flows** (``bool``) --
          If True, clear the sorted flows.

    """

    def __init__(self, dataset, root, use_existing=True, days=None, verbose=False, name=None,
                 export_mode=None, **kwargs):
        """
        Constructor for the base dataset configuration class.
        """

        self.dataset = dataset.name
        self.root = root

        if name is None:
            self.name = self.dataset
        else:
            self.name = name

        if export_mode is not None:
            self.config_root = export_mode
            self.export_mode = True
            self._get_existing_config()
            return
        # Export mode: If True, all paths must be considered relative from the config files location, rather than
        # absolute
        self.export_mode = False

        self.config_root = os.path.join(self.root, "config", self.name + ".json")

        if days is None:
            # We assume the configuration already exists, and that we don't have to create a new one
            if configuration_exists(self.name, self.root):
                self._get_existing_config()
            else:
                raise DatasetConfigurationConstructionException("Assumed existing configuration, but no configuration "
                                                                "was found. Please provide a value for \"days\".")

        self.days = days

        # Check if the configuration already exists:
        exists = configuration_exists(self.name, self.root)
        # If we have to use an existing configuration, use it
        if use_existing and exists:
            self._get_existing_config()
        # If a configuration exists, but if we should not use it, remove it and then
        # create a new one
        elif not use_existing and exists:
            remove_configuration(self.name, self.root, verbose=verbose)
            self._initial_configuration(**kwargs)
        # Else, just create a new configuration
        else:
            self._initial_configuration(**kwargs)

    def _get_config_dict(self):
        """
        Turn the current configuration into a dictionary.

        Returns:
        Current configuration as a dictinary
        """
        config = {}

        config['dataset'] = self.dataset
        config['root'] = self.root
        config['days'] = self.days
        config['name'] = self.name

        config['progress'] = {}
        config['progress']['dataset_present'] = self.dataset_present
        config['progress']['extraction_complete'] = self.extraction_complete
        config['progress']['sorting_complete'] = self.sorting_complete
        config['progress']['csv_preprocess_complete'] = self.csv_preprocess_complete
        config['progress']['started_labelling'] = self.started_labelling
        config['progress']['labelling_complete'] = self.labelling_complete
        config['progress']['days_completed'] = self.days_completed

        config['processing'] = {}
        config['processing']['use_c_libs'] = self.use_c_libs
        config['processing']['fast_sort'] = self.fast_sort
        config['processing']['bidirectional'] = self.bidirectional
        config['processing']['chronological'] = self.chronological
        config['processing']['batch_size'] = self.batch_size
        config['processing']['max_ram_usage'] = self.max_ram_usage
        config['processing']['include_ts'] = self.include_ts
        config['processing']['use_ts_labelling'] = self.use_ts_labelling
        config['processing']['avg_packet_size'] = self.avg_packet_size

        config['stream'] = {}
        config['stream']['stream_based'] = self.stream_based
        config['stream']['n_stream_flows'] = self.n_stream_flows
        config['stream']['n_stream_packets'] = self.n_stream_packets
        config['stream']['dynamic_stream_queue'] = self.dynamic_stream_queue

        config['storage'] = {}
        config['storage']['data_root'] = self.data_root
        config['storage']['config_root'] = self.config_root

        config['clear'] = {}
        config['clear']['clear_pcaps'] = self.clear_pcaps
        config['clear']['clear_csvs'] = self.clear_csvs
        config['clear']['clear_raw_packets'] = self.clear_raw_packets
        config['clear']['clear_sorted_flows'] = self.clear_sorted_flows
        config['clear']['prune_sorted_flows'] = self.prune_sorted_flows

        return config

    def _initial_configuration(self, **kwargs):
        """
        Set the data members to their intial value, whether default or specified by the keyword arguments.
        """
        # Processing progress
        self.dataset_present = {}
        self.extraction_complete = {}
        self.sorting_complete = {}
        self.csv_preprocess_complete = {}
        self.started_labelling = {}
        self.labelling_complete = {}
        self.days_completed = {}
        for day in self.days:
            self.dataset_present[day] = False
            self.extraction_complete[day] = False
            self.sorting_complete[day] = False
            self.csv_preprocess_complete[day] = False
            self.started_labelling[day] = False
            self.labelling_complete[day] = False
            self.days_completed[day] = False
        # Storage information:
        self.data_root = os.path.join(self.root, self.name)
        if not os.path.isdir(self.data_root):
            os.mkdir(self.data_root)

        # Processing configuration
        # use_c_libs = Whether to use C optimized functions
        self.use_c_libs = kwargs.get('use_c_libs', False)
        # fast_sort = Whether to extract and sort packets in one step instead of two
        self.fast_sort = kwargs.get('fast_sort', True)
        # bidirectional: When considering a flow with forward (fwd) and backward (bwd) packets, also consider the backward packets
        self.bidirectional = kwargs.get('bidirectional', True)
        # chronological: Combine fwd and bwd packets into one flow, taking into consideration the timing of each packet
        # By using this timing information, the fwd and bwd packets are sorted chronologically in one file before labelling,
        # rather than labelling two files seperately
        self.chronological = kwargs.get('chronological', False)
        # include_ts: Include the timestamps provided in the pcap file during the dataset preprocessing
        self.include_ts = kwargs.get('include_ts', True)
        # use_ts_labelling: Use the included timestamp as additional information when labelling packets
        self.use_ts_labelling = kwargs.get('use_ts_labelling', True) and self.include_ts

        # stream_based: Process the packet file as a stream of packets
        self.stream_based = kwargs.get('stream_based', False)
        self.n_stream_flows = kwargs.get('n_stream_flows', 5)
        self.n_stream_packets = kwargs.get('n_stream_packets', 5)
        self.dynamic_stream_queue = kwargs.get('dynamic_stream_queue', True)

        # Statistics when processing batches of packets
        self.batch_size = kwargs.get('batch_size', 1000000)
        self.max_ram_usage = kwargs.get('max_ram_usage', 7 * 1024 * 1024 * 1024)
        self.avg_packet_size = kwargs.get('avg_packet_size', 1714)  # CICIDS2017 friday

        # Storage clearing configuration
        # clear_raw_packets = Clear pcaps from storage after processing
        self.clear_pcaps = kwargs.get('clear_pcaps', False)
        # clear_raw_packets = CSV label files from storage after processing
        self.clear_csvs = kwargs.get('clear_csvs', False)
        # clear_raw_packets = Clear raw packets from storage after processing
        self.clear_raw_packets = kwargs.get('clear_raw_packets', False)
        # clear_raw_packets = Clear sorted flows from storage after processing
        self.clear_sorted_flows = kwargs.get('clear_sorted_flows', False)
        # prue_sorted_flows = Remove unlabelled flows after labelling
        self.prune_sorted_flows = kwargs.get('prune_sorted_flows', True)
        # Save this configuration
        self._store_configuration()

    def _update_from_existing_config(self, config):
        """
        Update the data members of the object by reading their correct state
        from the stored configuration.

        :arg config: Configuration read from storage, containing all required information.
        :type config: ``dict``
        config(dict): Stored configuration dictionary
        """
        self.days = config['days']

        self.dataset_present = config['progress']['dataset_present']
        self.extraction_complete = config['progress']['extraction_complete']
        self.sorting_complete = config['progress']['sorting_complete']
        self.csv_preprocess_complete = config['progress']['csv_preprocess_complete']
        self.started_labelling = config['progress']['started_labelling']
        self.labelling_complete = config['progress']['labelling_complete']
        self.days_completed = config['progress']['days_completed']

        self.use_c_libs = config['processing']['use_c_libs']
        self.fast_sort = config['processing']['fast_sort']
        self.bidirectional = config['processing']['bidirectional']
        self.chronological = config['processing']['chronological']
        self.batch_size = config['processing']['batch_size']
        self.max_ram_usage = config['processing']['max_ram_usage']
        self.include_ts = config['processing']['include_ts']
        self.use_ts_labelling = config['processing']['use_ts_labelling']
        self.avg_packet_size = config['processing']['avg_packet_size']

        self.stream_based = config['stream']['stream_based']
        self.n_stream_flows = config['stream']['n_stream_flows']
        self.n_stream_packets = config['stream']['n_stream_packets']
        self.dynamic_stream_queue = config['stream']['dynamic_stream_queue']

        self.data_root = config['storage']['data_root']
        self.config_root = config['storage']['config_root']

        self.clear_pcaps = config['clear']['clear_pcaps']
        self.clear_csvs = config['clear']['clear_csvs']
        self.clear_raw_packets = config['clear']['clear_raw_packets']
        self.clear_sorted_flows = config['clear']['clear_sorted_flows']
        self.prune_sorted_flows = config['clear']['prune_sorted_flows']

    def get_sorted_flows_root(self, day):
        """
        Get the root directory of the sorted flows for a specific day.

        :arg day: Day for which the root directory of the sorted flows is required.
        :type day: ``str``
        :return: Path to the root directory containing the sorted flows for the specified day.
        """
        return os.path.join(self.data_root, "sorted_flows", day)

    def get_labelled_days(self):
        """
        Get all days for which the flows are labelled.

        :return: List of all days with labelled flows.
        :rtype: ``list`` of ``str``
        """
        labelled_days = []

        for day in self.days:
            if self.labelling_complete[day]:
                labelled_days.append(day)

        return labelled_days

    def get_labels(self, day=None, only_labelled_days=False):
        """
        Get the labels for all traffic classes present in this dataset if no day is given,
        or for the specified day.

        :param day: Optional, day for which the labels must be acquired?
        :type day: ``str``
        :param only_labelled_days: If True, only days for which flows have been labelled (= for which dataset \
        preprocessing has completed) will be included when a day is not provided.
        :type only_labelled_days: ``bool``
        :return: List of labels of the present traffic classes
        :rtype: ``list`` of ``str``
        """
        return []

    def get_attacks(self, day=None):
        """
        Get the labels for all attack traffic classes present in this dataset if no day is given,
        or for the specified day.

        :param day: Optional, day for which the labels must be acquired?
        :type day: ``str``
        :return: List of labels of the present traffic classes
        :rtype: ``list`` of ``str``
        """
        return []

    def is_completed(self):
        """
        Check whether the dataset has been completely extracted, sorted and labelled.
        :return: True if the dataset has been completely extracted, sorted and labelled.
        :rtype: ``bool``
        """

        for day in self.days:
            if not self.days_completed[day]:
                return False
        return True

    def get_link_layer(self):
        """
        Get the Link Layer protocol used in the dataset. Default is Ethernet.

        :return: Used Link Layer protocol
        :rtype: ``LinkLayer``
        """
        return LinkLayer.ETHERNET

    def get_normal_class(self):
        """
        Get the class label of the dataset representing benign, non-malicious, regular or normal traffic.

        :return: Class label of the 'normal' class
        :rtype: ``str``
        """
        return ''


class ISCX2012Config(DatasetConfig):
    """
    Configuration for the ISCX2012 dataset (https://www.unb.ca/cic/datasets/ids.html).
    In addition to the base configuration, this class takes into account the XML label files charateristic for ISCX2012.

    :arg root: The root directory containing all datasets.
    :arg use_existing: If True, use an existing form of the dataset if possible. Otherwise, erase any existing data for this dataset and start from scratch.
    :arg days: List containing all days featured in the dataset.
    :arg verbose: If True, the function can output additional printed information.
    :type root: ``str``
    :type use_existing: ``bool``
    :type days: ``list`` of ``str``
    :type verbose: ``bool``

    :param \**kwargs: Arbitrary keyword arguments, as specified in :class:`DatasetConfig`.

    """

    def __init__(self, root, use_existing=True, days=None, verbose=False, **kwargs):
        super().__init__(dataset=Dataset.ISCX2012, root=root, use_existing=use_existing, days=days, \
                         verbose=verbose, **kwargs)

    def _initial_configuration(self, **kwargs):
        """
        For ISCX2012, some additional XML management is required, as the original labelled data is provided
        as XML files rather than CSV files.
        """
        # For ISCX2012 specifically
        self.xml_preprocessed = {}
        self.xml_processed = {}
        for day in self.days:
            self.xml_preprocessed[day] = False
            self.xml_processed[day] = False

        super()._initial_configuration(**kwargs)

    def _get_config_dict(self):

        config = super()._get_config_dict()
        config['xml'] = {
            'xml_preprocessed': self.xml_preprocessed,
            'xml_processed': self.xml_processed
        }

        return config

    def _update_from_existing_config(self, config):
        super()._update_from_existing_config(config)

        self.xml_preprocessed = config['xml']['xml_preprocessed']
        self.xml_processed = config['xml']['xml_processed']

    def get_xml_filenames(self, day):
        """
        Get the names of the XML files containing the dataset information per day, as obtained via download.

        :arg day: Day for which the XML filenames are require. Must contain number between "11" and "17", the period for this dataset.
        :type day: ``str``

        :return: List of filenames corresponding with the label XML files of the given day.
        :rtype: ``list`` of ``str``
        """
        if "11" in day:
            return []
        elif "12" in day:
            return ["TestbedSatJun12Flows.xml"]
        elif "13" in day:
            return ["TestbedSunJun13Flows.xml"]
        elif "14" in day:
            return ["TestbedMonJun14Flows.xml"]
        elif "15" in day:
            return ["TestbedTueJun15-1Flows.xml", "TestbedTueJun15-2Flows.xml", "TestbedTueJun15-3Flows.xml"]
        elif "16" in day:
            return ["TestbedWedJun16-1Flows.xml", "TestbedWedJun16-2Flows.xml", "TestbedWedJun16-3Flows.xml"]
        elif "17" in day:
            return ["TestbedThuJun17-1Flows.xml", "TestbedThuJun17-2Flows.xml", "TestbedThuJun17-3Flows.xml"]
        else:
            return None

    def get_corrupted_xml_lines(self, filename):
        """
        In the downloaded XML label file TestbedThuJun17-1Flows.xml, 3 data lines are corrupted. These need to be remove
        before feeding the file to an XML parser.

        :arg filename: Name of the file for which corrupted lines are sought.
        :type filename: ``str``

        :return: List of corrupted line numbers, if any.
        :rtype: ``list`` of ``int``
        """
        if filename == "TestbedThuJun17-1Flows.xml":
            return [3135759, 3315542, 3587308, 3755344]
        else:
            return []

    def get_attacks(self, day=None):
        """
        For a specific day, get the ISCX2012 attack class, or get all attack classes if no day is specified.

        :param day: Day of the ISCX2012 week, should be between 11 and 17 and must contain the number, optional.
        :type day: ``str``
        :return: Attack class of that day, or of all days
        :rtype: ``list`` of ``str``
        """
        if day is None:
            return ["BFSSH", "Infiltration", "DoS", "DDoS"]
        else:
            if "11" in day:
                return ""
            elif "12" in day:
                return ["BFSSH"]
            elif "13" in day:
                return ["Infiltration"]
            elif "14" in day:
                return ["DoS"]
            elif "15" in day:
                return ["DDoS"]
            elif "16" in day:
                return ["BFSSH"]
            elif "17" in day:
                return ["BFSSH"]
            else:
                return None

    def get_labels(self, day=None, only_labelled_days=False):
        """
        Get the labels for all traffic classes present in this dataset if no day is given,
        or for the specified day.

        :param day: Optional, day for which the labels must be acquired?
        :type day: ``str``
        :param only_labelled_days: If True, only days for which flows have been labelled (= for which dataset \
        preprocessing has completed) will be included when a day is not provided.
        :type only_labelled_days: ``bool``
        :return: List of labels of the present traffic classes
        :rtype: ``list`` of ``str``
        """
        if day is None:
            if only_labelled_days:
                labels = ['Normal']

                for day in self.get_labelled_days():
                    labels += self.get_attacks(day)
                return labels
            else:
                return ["Normal", "BFSSH", "Infiltration", "DoS", "DDoS"]
        else:
            attack = self.get_attacks(day)
            if attack != "":
                return ["Normal", attack]
            else:
                return ["Normal"]

    def get_link_layer(self):
        """
        Get the Link Layer protocol used in the dataset.

        :return: Used Link Layer protocol
        :rtype: ``LinkLayer``
        """
        return LinkLayer.ETHERNET

    def get_normal_class(self):
        """
        Get the class label of the dataset representing benign, non-malicious, regular or normal traffic.

        :return: Class label of the 'normal' class
        :rtype: ``str``
        """
        return "Normal"


class UNSW_NB15Config(DatasetConfig):
    """
    Configuration for the UNSW-NB15 dataset:

    :arg root: The root directory containing all datasets.
    :arg use_existing: If True, use an existing form of the dataset if possible. Otherwise, erase any existing data for this dataset and start from scratch.
    :arg days: List containing all days featured in the dataset.
    :arg verbose: If True, the function can output additional printed information.
    :type root: ``str``
    :type use_existing: ``bool``
    :type days: ``list`` of ``str``
    :type verbose: ``bool``

    :param \**kwargs: Arbitrary keyword arguments, as specified in :class:`DatasetConfig`.

    Additional support for downloading the dataset files was added.
    This means that, on top of the keyword arguments supported by :class:`DatasetConfig`, the following keyword
    arguments are also available:

    :Keyword Arguments:
        * **use_download** (``bool``) --
          If True, download the dataset files during the processing, rather than using predownloaded files.
        * **only_tcp_udp** (``bool``) --
          If True, only label tcp and udp flows.

    """

    def __init__(self, root, use_existing=True, days=None, verbose=False, **kwargs):
        """
        Constructor for UNSW_NB15 configuration objects.
        """
        super().__init__(dataset=Dataset.UNSW_NB15, root=root, use_existing=use_existing, days=days, \
                         verbose=verbose, **kwargs)

    def _initial_configuration(self, **kwargs):
        """
        For UNSW-NB15, we need some additional downloading management.

        :param \**kwargs: Arbitrary keyword arguments, as specified above.
        """

        # Download configuration
        self.use_download = kwargs.get('use_download', False)
        # Download status contains urls as well as their download status (aka, True if the target has been downloaded)
        self.download_status_labels = {}
        self.download_status_pcaps = {}

        self.pcaps_22_1 = 53
        self.pcaps_17_2 = 27

        self.only_tcp_udp = kwargs.get('only_tcp_udp', True)

        for day in self.days:
            # Calculate CSV download commands
            target_dir_labels = os.path.join(self.root, self.dataset, "csv_data", day)
            self.download_status_labels[day] = self._get_download_data_labels(day, target_dir_labels)

            # Calculate PCAP download commands
            target_dir_pcaps = os.path.join(self.root, self.dataset, "pcap_data", day)
            self.download_status_pcaps[day] = self._get_download_data_pcaps(day, target_dir_pcaps)

        super()._initial_configuration(**kwargs)

    def _get_config_dict(self):
        """
        Get a dictionary containing the configuration of this dataset. This dictionary can be used for storage.

        :return: Dictionary containing the configuration of this dataset.
        :rtype: ``dict``
        """
        config = super()._get_config_dict()
        config['download'] = {}
        config['download']['use_download'] = self.use_download
        config['download']['download_status_labels'] = self.download_status_labels
        config['download']['download_status_pcaps'] = self.download_status_pcaps
        config['download']['pcaps_22_1'] = self.pcaps_22_1
        config['download']['pcaps_17_2'] = self.pcaps_17_2

        config['only_tcp_udp'] = self.only_tcp_udp
        return config

    def _update_from_existing_config(self, config):
        """
        Use an existing dictionary to update the configuration of the dataset.

        :arg config: Existing configuration that will be used to update this configuration.
        :type config: ``dict``
        """
        super()._update_from_existing_config(config)

        self.use_download = config['download']['use_download']
        self.download_status_labels = config['download']['download_status_labels']
        self.download_status_pcaps = config['download']['download_status_pcaps']
        self.pcaps_22_1 = config['download']['pcaps_22_1']
        self.pcaps_17_2 = config['download']['pcaps_17_2']
        self.only_tcp_udp = config['only_tcp_udp']

    def _get_download_data_labels(self, day_string, target_dir):
        """
        Get the download commands to download the label CSVs for the UNSW-NB15 dataset.

        :arg day_string: String containing the date for which the download data is required. Must contain "22-01, 22-1, 17-02 or 17-2".
        :arg target_dir: Path to directory to store downloaded csv label files into.
        :type day_string: ``str``
        :type target_dir: ``str``
        :return: List of wget download commands that can be executed in the command line to download the label csv files.
        :rtype: ``list`` of ``str``
        """
        if "22-01" in day_string:
            day_string = "22-1"
        elif "17-02" in day_string:
            day_string = "17-2"
        is_day_1 = ("22-1" in day_string)
        is_day_2 = ("17-2" in day_string)
        if not (is_day_1 or is_day_2):
            raise DatasetPreprocessInvalidArgument(
                "There are not PCAPs to be downloaded for the specified date of " + day_string + \
                "-2015. Please choose either 22-1-2015 or 17-2-2015.")
        base_urls = [
            "https://cloudstor.aarnet.edu.au/plus/s/2DhnLGDdEECo4ys/download?path=%2FUNSW-NB15%20-%20CSV%20Files&files=UNSW-NB15_1.csv", \
            "https://cloudstor.aarnet.edu.au/plus/s/2DhnLGDdEECo4ys/download?path=%2FUNSW-NB15%20-%20CSV%20Files&files=UNSW-NB15_2.csv", \
            "https://cloudstor.aarnet.edu.au/plus/s/2DhnLGDdEECo4ys/download?path=%2FUNSW-NB15%20-%20CSV%20Files&files=UNSW-NB15_3.csv", \
            "https://cloudstor.aarnet.edu.au/plus/s/2DhnLGDdEECo4ys/download?path=%2FUNSW-NB15%20-%20CSV%20Files&files=UNSW-NB15_4.csv"]

        # -q = Quiet
        # tail = " -q"
        tail = ""

        if is_day_1:
            cmd1 = "wget \"" + base_urls[0] + "\" -O \"" + target_dir + "/" + "UNSW-NB15_1.csv" + "\"" + tail
            cmd2 = "wget \"" + base_urls[1] + "\" -O \"" + target_dir + "/" + "UNSW-NB15_2.csv" + "\"" + tail
            return [("UNSW-NB15_1.csv", cmd1, False), ("UNSW-NB15_2.csv", cmd2, False)]
        else:
            cmd1 = "wget \"" + base_urls[2] + "\" -O \"" + target_dir + "/" + "UNSW-NB15_3.csv" + "\"" + tail
            cmd2 = "wget \"" + base_urls[3] + "\" -O \"" + target_dir + "/" + "UNSW-NB15_4.csv" + "\"" + tail
            return [("UNSW-NB15_3.csv", cmd1, False), ("UNSW-NB15_4.csv", cmd2, False)]

    def _get_download_data_pcaps(self, day_string, target_dir):
        """
        Get the download commands to download the pcap files for the UNSW-NB15 dataset.

        :arg day_string: String containing the date for which the download data is required. Must contain "22-01, 22-1, 17-02 or 17-2".
        :arg target_dir: Path to directory to store downloaded pcap files into.
        :type day_string: ``str``
        :type target_dir: ``str``
        :return: List of wget download commands that can be executed in the command line to download the pcap files.
        :rtype: ``list`` of ``str``
        """

        day, month = day_string.split("-")

        # Remove leading 0's
        day = str(int(day))
        month = str(int(month))

        # Check if the days are valid
        is_day_1 = (int(day) == 22) and (int(month) == 1)
        is_day_2 = (int(day) == 17) and (int(month) == 2)

        if not (is_day_1 or is_day_2):
            raise DatasetPreprocessInvalidArgument(
                "There are not PCAPs to be downloaded for the specified date of " + day + "-" + month + \
                "-2015. Please choose either 22-1-2015 or 17-2-2015.")

        base_url = "\"https://cloudstor.aarnet.edu.au/plus/s/2DhnLGDdEECo4ys/download?path=%2FUNSW-NB15%20-%20pcap%20files%2Fpcaps%20" + \
                   day + "-" + month + "-2015&files="

        # -q = Quiet
        # tail = " -q"
        tail = ""

        if is_day_1:
            file_range = self.pcaps_22_1
        else:
            file_range = self.pcaps_17_2

        download_data = []
        for i in range(1, file_range + 1):
            filename = str(i) + ".pcap"
            cmd = "wget " + base_url + filename + "\" -O \"" + target_dir + "/" + filename + "\"" + tail
            download_data.append((filename, cmd, False))

        return download_data

    def update_pcaps_download_status(self, day, filename):
        """
        Update the download status of the specified filename at the specified date in the configuration.
        This function must be used to update the status of pcap files.

        :arg day: Day for which the file was (not) downloaded.
        :arg filename: Specific file that was (not) downloaded for that day, and whose download status needs to be updated.
        :type day: ``str``
        :type filename: ``str``
        """
        for (idx, (f, cmd, _)) in enumerate(self.download_status_pcaps[day]):
            if f == filename:
                self.download_status_pcaps[day][idx] = (f, cmd, True)
                self.update()
                break

    def update_labels_download_status(self, day, filename):
        """
        Update the download status of the specified filename at the specified date in the configuration.
        This function must be used to update the status of label csv files.

        :arg day: Day for which the file was (not) downloaded.
        :arg filename: Specific file that was (not) downloaded for that day, and whose download status needs to be updated.
        :type day: ``str``
        :type filename: ``str``
        """
        for (idx, (f, cmd, _)) in enumerate(self.download_status_labels[day]):
            if f == filename:
                self.download_status_labels[day][idx] = (f, cmd, True)
                self.update()
                break

    def get_csv_split_line(self):
        """
        For UNSW-NB15, the second csv label file contains flows of both 22-1 as well as 17-2, making it initially unusable for labelling.
        This function gives the line in the downloaded csv file (with added column names) starting from which the lines represent 17-2 flows.
        All flows before this point belong to 22-1.

        :return: Line number for UNSW-NB15_2.csv with added column names starting from which flows and labels belong to 17-2.
        :rtype: ``int``
        """
        return 387202

    def has_completed_downloading_pcaps(self, day):
        """
        For a specific day, check if all pcap files have been downloaded.

        :arg day: Day for which will be checked if all pcap files have been downloaded. Must be contained in `self.days`.
        :type day: ``str``

        :return: True if all pcap files have been downloaded for the specific day.
        :rtype: ``bool``
        """
        for _, _, status in self.download_status_pcaps[day]:
            if not status:
                return False
        return True

    def has_completed_downloading_labels(self, day):
        """
        For a specific day, check if all label csv files have been downloaded.

        :arg day: Day for which will be checked if all label csv files have been downloaded. Must be contained in `self.days`.
        :type day: ``str``

        :return: True if all label csv files have been downloaded for the specific day.
        :rtype: ``bool``
        """
        for _, _, status in self.download_status_labels[day]:
            if not status:
                return False
        return True

    def get_labels(self, day=None, only_labelled_days=False):
        """
        Get the labels for all traffic classes present in this dataset if no day is given,
        or for the specified day.

        :param day: Optional, day for which the labels must be acquired?
        :type day: ``str``
        :param only_labelled_days: If True, only days for which flows have been labelled (= for which dataset \
        preprocessing has completed) will be included when a day is not provided.
        :type only_labelled_days: ``bool``
        :return: List of labels of the present traffic classes
        :rtype: ``list`` of ``str``
        """
        return ["Normal", "Fuzzers", "Analysis", "Backdoors", "Exploits", "Reconnaissance", "DoS", "Generic",
                "Shellcode", "Worms"]

    def get_attacks(self, day=None):
        """
        Get the attack classes for the specified day in UNSW-NB15, or simply all attacks

        :param day: Specified day
        :type day: ``str``
        :return: List of attack labels
        :rtype: ``list`` of ``str``
        """
        # Plot twist: the attacks labels are the same for both days.
        return ["Fuzzers", "Analysis", "Backdoors", "Exploits", "Reconnaissance", "DoS", "Generic", "Shellcode",
                "Worms"]

    def get_link_layer(self):
        """
        Get the Link Layer protocol used in the dataset.

        :return: Used Link Layer protocol
        :rtype: ``LinkLayer``
        """
        return LinkLayer.LCC

    def get_normal_class(self):
        """
        Get the class label of the dataset representing benign, non-malicious, regular or normal traffic.

        :return: Class label of the 'normal' class
        :rtype: ``str``
        """
        return "Normal"


class CICIDS2017Config(DatasetConfig):
    """
    Configuration for the CICIDS2017 dataset.

    :arg root: The root directory containing all datasets.
    :arg use_existing: If True, use an existing form of the dataset if possible. Otherwise, erase any existing data for this dataset and start from scratch.
    :arg days: List containing all days featured in the dataset.
    :arg verbose: If True, the function can output additional printed information.
    :type root: ``str``
    :type use_existing: ``bool``
    :type days: ``list`` of ``str``
    :type verbose: ``bool``

    :param \**kwargs: Arbitrary keyword arguments, as specified in :class:`DatasetConfig`.
    """

    def __init__(self, root, use_existing=True, days=None, verbose=False, **kwargs):
        super().__init__(dataset=Dataset.CICIDS2017, root=root, use_existing=use_existing, days=days, \
                         verbose=verbose, **kwargs)

    def get_labels(self, day=None, only_labelled_days=False):
        """
        Get the labels for all traffic classes present in this dataset if no day is given,
        or for the specified day.

        :param day: Optional, day for which the labels must be acquired?
        :type day: ``str``
        :param only_labelled_days: If True, only days for which flows have been labelled (= for which dataset \
        preprocessing has completed) will be included when a day is not provided.
        :type only_labelled_days: ``bool``
        :return: List of labels of the present traffic classes
        :rtype: ``list`` of ``str``
        """
        if day is None:
            if only_labelled_days:
                labels = ["BENIGN"]
                for day in self.get_labelled_days():
                    labels += self.get_attacks(day)
                return labels
            else:
                return ["BENIGN", "Bot", "PortScan", "DDoS", "Web Attack Brute Force", "Web Attack Sql Injection",
                        "Web Attack XSS", "Infiltration", "DoS GoldenEye", "DoS Hulk", "DoS Slowhttptest",
                        "DoS slowloris", "Heartbleed", "FTP-Patator", "SSH-Patator"]
        else:
            if day == "friday":
                return ["BENIGN", "Bot", "PortScan", "DDoS"]
            elif day == "thursday":
                return ["BENIGN", "Web Attack Brute Force", "Web Attack Sql Injection", "Web Attack XSS",
                        "Infiltration"]
            elif day == "wednesday":
                return ["BENIGN", "DoS GoldenEye", "DoS Hulk", "DoS Slowhttptest", "DoS slowloris", "Heartbleed"]
            elif day == "tuesday":
                return ["BENIGN", "FTP-Patator", "SSH-Patator"]
            elif day == "monday":
                return ["BENIGN"]
            else:
                return []

    def get_attacks(self, day=None):
        """
        For a specific day, get the ISCX2012 attack class, or get all attack classes if no day is specified.

        :param day: Day of the ISCX2012 week, should be between 11 and 17 and must contain the number, optional.
        :type day: ``str``
        :return: Attack class of that day, or of all days
        :rtype: ``list`` of ``str``
        """
        if day is None:
            return ["Bot", "PortScan", "DDoS", "Web Attack Brute Force", "Web Attack Sql Injection",
                    "Web Attack XSS", "Infiltration", "DoS GoldenEye", "DoS Hulk", "DoS Slowhttptest",
                    "DoS slowloris", "Heartbleed", "FTP-Patator", "SSH-Patator"]
        else:
            if day == "friday":
                return ["Bot", "PortScan", "DDoS"]
            elif day == "thursday":
                return ["Web Attack Brute Force", "Web Attack Sql Injection", "Web Attack XSS",
                        "Infiltration"]
            elif day == "wednesday":
                return ["DoS GoldenEye", "DoS Hulk", "DoS Slowhttptest", "DoS slowloris", "Heartbleed"]
            elif day == "tuesday":
                return ["FTP-Patator", "SSH-Patator"]
            elif day == "monday":
                return []
            else:
                return None

    def get_normal_class(self):
        """
        Get the class label of the dataset representing benign, non-malicious, regular or normal traffic.

        :return: Class label of the 'normal' class
        :rtype: ``str``
        """
        return "BENIGN"
