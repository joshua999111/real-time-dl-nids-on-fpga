from enum import Enum


class FeatureStrategy(Enum):
    PCCN = 1
    HAST_I = 2
    HAST_II = 3
    SIMPLE_VECTORS = 4
    SESSIONBASED = 5


class Dataset(Enum):
    CICIDS2017 = 1
    DARPA1998 = 2
    UNSW_NB15 = 3
    ISCX2012 = 4


class PacketSource(Enum):
    BINARY = 1
    BYTEARR = 2
    STRING = 3


class DataType(Enum):
    UINT8 = 1,
    FLOAT32 = 2,


class LinkLayer(Enum):
    ETHERNET = 1
    LCC = 2