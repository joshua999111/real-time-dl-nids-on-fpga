"""
This module contains functions that are generally helpful and that can be/are used at multiple places in the framework.
"""

import os
import numpy as np


def remove_dir(root, remove_root=True, verbose=True):
    """
    Remove target root directory. If remove_root is True, remove the contents of the root directory,
    and then the directory itself. Otherwise, only remove the content.

    :param root: Target root directory.
    :param remove_root: If True, remove the root directory too.
    :param verbose: If True, output the removed files and directories
    :type root: ``str``
    :type remove_root: ``bool``
    :type verbose: ``bool``
    :return: None
    """
    # Remove the content of the directory
    __remove_dir_recursively(root, verbose)

    # If required, remove the root itself
    if remove_root:
        os.rmdir(root)


def __remove_dir_recursively(root, verbose=True):
    """
    Recursive function used to remove files and directories following ``remove_dir``.
    :param root: Root of which contents need to be removed.
    :param verbose: If True, output the removed files and directories
    :type root: ``str``
    :type verbose: ``bool``
    :return: None
    """
    # Get the content of the 
    for target in os.listdir(root):
        target_path = os.path.join(root, target)
        # If the target is a file, remove the file
        if os.path.isfile(target_path):
            if verbose: print("Removing", target_path)
            os.remove(target_path)
        elif os.path.isdir(target_path):
            # Empty the directory
            __remove_dir_recursively(target_path, verbose=verbose)
            # If it is empty, remove it
            if len(os.listdir(target_path)) < 1:
                if verbose:
                    print("Removing", target_path)
                os.rmdir(target_path)


def mkdir(path):
    """
    Function that creates the given directory, if it does not already exist.
    Otherwise, nothing happens.
    :param path: Path to the directory that needs to be created.
    :return: None
    """
    if not os.path.isdir(path):
        os.mkdir(path)


def _split_path_recursive(path, results):
    """
    Split the provided path into its different parts.

    :param path: Path to split
    :param results: Temporary list containing the current results
    :return: List containing the different parts of the path
    :rtype: ``list`` of ``str``
    """
    if path == "":
        return results
    else:
        path, item = os.path.split(path)
        if path == '/':
            return [path, item] + results
        else:
            return _split_path_recursive(path, [item] + results)


def split_path(path):
    """
    Split a path into its different parts, e.g. 'alpha/beta/gamma/delta.txt' will become
    ['alpha', 'beta', 'gamma', 'delta.txt'].

    :param path: The path to split
    :type path: ``str``
    :return: A list containing the different parts of the path
    :rtype: ``list`` of ``str``
    """
    return _split_path_recursive(path, [])


def extract_hast_ohe_matrix(vector):
    """
    Turn the provided vector with values between 0 and 255 into a One-Hot Encoded matrix containing the OHE-vectors
    corresponding with each value in the provided vector.

    :param vector: Provided (n x 1) vector.
    :type vector: ``numpy.ndarray``
    :return: Corresponding (256 x n) OHE array
    :rtype: ``numpy.ndarray``
    """
    # input: vector of size n
    # output: matrix of size 256 x n
    output = np.zeros((256, vector.shape[0]))

    for i in range(vector.shape[0]):
        output[:, i] = ohe_byte(vector[i])

    return output


def ohe_byte(byte):
    """
    One-hot encode the input byte into a 256-dimensional vector

    :param byte: Byte to be one-hot encoded
    :type byte: ``int``
    :return: one-hot encoded 256D vector with '1' corresponding to the value of the byte
    :rtype: ``Numpy Array``
    """
    vector = np.zeros(256)
    vector[byte] = 1
    return vector


def shape_to_str(shape):
    """
    Turn a shape tuple into a string representation.

    :param shape: The shape tuple
    :type shape: ``tuple`` of ``int``
    :return: String representation
    :rtype: ``str``
    """
    return '_'.join([str(i) for i in shape])


def str_to_shape(shape_str):
    """
    Turn a string representing a shape back into the original shape.

    :param shape_str: String representing a shape tuple
    :type shape_str: ``str``
    :return: Original shape tuple
    :rtype: ``tuple`` of ``int``
    """
    return tuple([int(i) for i in shape_str.split('_')])
