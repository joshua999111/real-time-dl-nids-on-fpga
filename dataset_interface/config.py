import os
import json


def configuration_exists(name, root):
    """
    Check if, for a given dataset, a configuration already exists.

    :arg name: The name of the required configuration
    :arg root: Path to the root directory containing the configuration folder.
    :type name: ``str``
    :type root: ``str``

    :return: Returns `True` if a configuration exists for the specified name.
    :rtype: ``bool``
    """
    target = os.path.join(root, "config", name + ".json")
    return os.path.isfile(target)


class Config(object):
    """

    """
    def __init__(self, root, name):

        self.root = root
        self.name = name
        self.config_root = os.path.join(root, "config", name)

    def update(self):
        """
        Store the current configuration, so that it may be loaded in at a later time. Use this function to save
        changes to the configuration.
        """
        self._store_configuration()

    def save(self):
        """
        Store the current configuration, so that it may be loaded in at a later time. Use this function to save
        changes to the configuration.
        """
        self._store_configuration()

    def _get_existing_config(self):
        """
        Get the existing configuration for this dataset by loading the .json storage file and
        setting the data members accordingly.
        """
        config = self._get_existing_config_dict()
        self._update_from_existing_config(config)

    def _get_existing_config_dict(self):
        """
        Load the existing configuration from the storage .json file as a dictionary.

        Returns:
        Existing configuration as a dict
        """
        with open(self.config_root, 'r') as infile:
            return json.load(infile)

    def _update_from_existing_config(self, config):
        """
        Update the data members of the object by reading their correct state
        from the stored configuration.

        Params:
        config(dict): Stored configuration dictionary
        """
        pass

    def _get_config_dict(self):
        return {}

    def _store_configuration(self):
        """
        Store the current configuration in a .json file.
        """
        config = self._get_config_dict()
        self._store_current_config_dict(config)

    def _store_current_config_dict(self, config):
        """
        Store the current configuration as a dict in a .json file.

        Params:
        config(dict): Current configuration as a dict.
        """
        with open(self.config_root, "w") as f:
            json.dump(config, f)
