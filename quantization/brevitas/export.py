import torch
import brevitas.onnx as bo
from learning.prerequisites import get_net, get_input_shape


def export_to_finn_onnx(experiment, output_path):
    """

    """

    # Load the model
    neural_net = get_net(experiment)
    neural_net.load_state_dict(torch.load(experiment.best_model_path))

    neural_net.eval()

    # Export the model
    print("Exporting model")
    print("Input shape: {}".format(get_input_shape(experiment)))
    bo.export_finn_onnx(neural_net, get_input_shape(experiment), output_path)
