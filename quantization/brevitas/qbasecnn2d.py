import torch
from torch.nn import Module, ModuleList, BatchNorm2d, MaxPool2d, BatchNorm1d, AdaptiveAvgPool2d

from brevitas.nn import QuantConv2d, QuantIdentity, QuantLinear, QuantReLU, QuantAvgPool2d, QuantMaxPool2d

# Settings
from brevitas.core.quant import QuantType
from brevitas.core.restrict_val import RestrictValueType
from brevitas.core.scaling import ScalingImplType
from brevitas.core.stats import StatsOp


QUANT_TYPE = QuantType.INT
SCALING_MIN_VAL = 2e-16

ACT_SCALING_IMPL_TYPE = ScalingImplType.PARAMETER
ACT_SCALING_PER_CHANNEL = False
ACT_SCALING_RESTRICT_SCALING_TYPE = RestrictValueType.LOG_FP
ACT_MAX_VAL = 6.0
ACT_RETURN_QUANT_TENSOR = False
ACT_PER_CHANNEL_BROADCASTABLE_SHAPE = None
HARD_TANH_THRESHOLD = 10.0

WEIGHT_SCALING_IMPL_TYPE = ScalingImplType.STATS
WEIGHT_SCALING_PER_OUTPUT_CHANNEL = True
WEIGHT_SCALING_STATS_OP = StatsOp.MAX
WEIGHT_RESTRICT_SCALING_TYPE = RestrictValueType.LOG_FP
WEIGHT_NARROW_RANGE = True

ENABLE_BIAS_QUANT = False

HADAMARD_FIXED_SCALE = False


def make_quant_conv2d(in_channels,
                      out_channels,
                      kernel_size,
                      stride,
                      padding,
                      groups,
                      bias,
                      bit_width,
                      enable_bias_quant=ENABLE_BIAS_QUANT,
                      weight_quant_type=QUANT_TYPE,
                      weight_scaling_impl_type=WEIGHT_SCALING_IMPL_TYPE,
                      weight_scaling_stats_op=WEIGHT_SCALING_STATS_OP,
                      weight_scaling_per_output_channel=WEIGHT_SCALING_PER_OUTPUT_CHANNEL,
                      weight_restrict_scaling_type=WEIGHT_RESTRICT_SCALING_TYPE,
                      weight_narrow_range=WEIGHT_NARROW_RANGE,
                      weight_scaling_min_val=SCALING_MIN_VAL):
    bias_quant_type = QUANT_TYPE if enable_bias_quant else QuantType.FP
    return QuantConv2d(in_channels,
                       out_channels,
                       groups=groups,
                       kernel_size=kernel_size,
                       padding=padding,
                       stride=stride,
                       bias=bias,
                       bias_quant_type=bias_quant_type,
                       compute_output_bit_width=bias and enable_bias_quant,
                       compute_output_scale=bias and enable_bias_quant,
                       weight_bit_width=bit_width,
                       weight_quant_type=weight_quant_type,
                       weight_scaling_impl_type=weight_scaling_impl_type,
                       weight_scaling_stats_op=weight_scaling_stats_op,
                       weight_scaling_per_output_channel=weight_scaling_per_output_channel,
                       weight_restrict_scaling_type=weight_restrict_scaling_type,
                       weight_narrow_range=weight_narrow_range,
                       weight_scaling_min_val=weight_scaling_min_val)


def make_quant_relu(bit_width,
                    quant_type=QUANT_TYPE,
                    scaling_impl_type=ACT_SCALING_IMPL_TYPE,
                    scaling_per_channel=ACT_SCALING_PER_CHANNEL,
                    restrict_scaling_type=ACT_SCALING_RESTRICT_SCALING_TYPE,
                    scaling_min_val=SCALING_MIN_VAL,
                    max_val=ACT_MAX_VAL,
                    return_quant_tensor=ACT_RETURN_QUANT_TENSOR,
                    per_channel_broadcastable_shape=ACT_PER_CHANNEL_BROADCASTABLE_SHAPE):
    return QuantReLU(bit_width=bit_width,
                     quant_type=quant_type,
                     scaling_impl_type=scaling_impl_type,
                     scaling_per_channel=scaling_per_channel,
                     restrict_scaling_type=restrict_scaling_type,
                     scaling_min_val=scaling_min_val,
                     max_val=max_val,
                     return_quant_tensor=return_quant_tensor,
                     per_channel_broadcastable_shape=per_channel_broadcastable_shape)


class Conv2DBlock(Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=1, groups=1, bias=False,
                 eps=1e-5, weight_bit_width=8, act_bit_width=8):
        super(Conv2DBlock, self).__init__()

        self.conv2d = make_quant_conv2d(in_channels=in_channels, out_channels=out_channels, bit_width=weight_bit_width,
                                        kernel_size=kernel_size, stride=stride, padding=padding, groups=groups,
                                        bias=bias, enable_bias_quant=bias)
        self.bn = BatchNorm2d(out_channels, eps=eps)
        self.relu = make_quant_relu(bit_width=act_bit_width)

    def forward(self, x):
        x = self.conv2d(x)
        x = self.bn(x)
        x = self.relu(x)

        return x


class QBaseCNN2DMaxpool(Module):
    """
    This does not quite work:
    We need to call x.permute(0, 2, 3, 1) in order to transform the MaxPool into a MaxPoolNHWC in FINN
    However, this operation only works on torch.Tensor, not on QuantTensor (which we get out of a QuantMaxPool2d with
    return_quant_tensor=True). If we use a Tensor, the input of the FCL will not be quantized, which in turn will
    result in a layer that cannot be quantized by FINN (MatMul cannot go to QuantizedStreamingFCLayer).
    So, we either cannot use the MaxPool, or we cannot accelerate the first FC layer.

    """
    def __init__(self, num_classes, weight_bit_width, act_bit_width, in_ch):
        super(QBaseCNN2DMaxpool, self).__init__()

        self.block1 = ModuleList()
        self.block2 = ModuleList()
        self.block3 = ModuleList()
        self.block4 = ModuleList()

        self.block1.append(make_quant_conv2d(in_channels=in_ch, out_channels=16,
                                             bit_width=weight_bit_width, kernel_size=3, stride=1, padding=1,
                                             groups=1, bias=False))
        self.block1.append(make_quant_relu(bit_width=act_bit_width))
        self.block1.append(make_quant_relu(bit_width=act_bit_width))

        self.block2.append(make_quant_conv2d(in_channels=16, out_channels=32,
                                             bit_width=weight_bit_width, kernel_size=3, stride=2, padding=1,
                                             groups=1, bias=False))
        self.block2.append(make_quant_relu(bit_width=act_bit_width))
        self.block2.append(make_quant_relu(bit_width=act_bit_width))

        self.block3.append(make_quant_conv2d(in_channels=32, out_channels=96,
                                             bit_width=weight_bit_width, kernel_size=3, stride=1, padding=1,
                                             groups=1, bias=False))
        self.block3.append(make_quant_relu(bit_width=act_bit_width))
        self.block3.append(make_quant_relu(bit_width=act_bit_width))

        self.block4.append(make_quant_conv2d(in_channels=96, out_channels=256,
                                             bit_width=weight_bit_width, kernel_size=3, stride=2, padding=1,
                                             groups=1, bias=False))
        self.block4.append(make_quant_relu(bit_width=act_bit_width))
        self.block4.append(make_quant_relu(bit_width=act_bit_width))

        #self.maxpool = MaxPool2d(kernel_size=6)
        self.maxpool = QuantMaxPool2d(kernel_size=6, return_quant_tensor=False)

        self.quantid = QuantIdentity()

        self.fc = QuantLinear(in_features=256, out_features=num_classes, bias=False, weight_bit_width=weight_bit_width,
                               weight_quant_type=QUANT_TYPE)

        for m in self.modules():
            if isinstance(m, QuantConv2d) or isinstance(m, QuantLinear):
                torch.nn.init.uniform_(m.weight.data, -1, 1)

    def forward(self, x):

        for layer in self.block1:
            x = layer(x)

        for layer in self.block2:
            x = layer(x)

        for layer in self.block3:
            x = layer(x)

        for layer in self.block4:
            x = layer(x)

        x = self.maxpool(x)
        # NCHW to NHWC
        x = x.permute(0, 2, 3, 1)

        #x = x.view(1, 1, 1, 256)

        #x = x.reshape(1, 256)

        x = self.quantid(x)

        x = x.view(x.shape[0], -1)
        print(x.shape)
        #x = self.quantid(x)
        #print(x.shape)
        x = self.fc(x)

        return x


class QBaseCNN2DAvgpool(Module):

    def __init__(self, num_classes, weight_bit_width, act_bit_width, in_ch, config=None):
        super(QBaseCNN2DAvgpool, self).__init__()

        self.conv_blocks = ModuleList()

        if config is None:
            config = {
                'OUT_CH': [16, 32, 96, 256],
                'KERNEL_SIZE': [3, 3, 3, 3],
                'STRIDE': [1, 2, 1, 2],
                'PADDING': [1, 1, 1, 1],
            }

        current_in_ch = in_ch
        for i in range(len(config['OUT_CH'])):
            self.conv_blocks.append(Conv2DBlock(in_channels=current_in_ch,
                                                out_channels=config['OUT_CH'][i],
                                                kernel_size=config['KERNEL_SIZE'][i],
                                                stride=config['STRIDE'][i],
                                                groups=1,
                                                padding=config['PADDING'][i],
                                                bias=False,
                                                eps=1e-5,
                                                weight_bit_width=weight_bit_width,
                                                act_bit_width=act_bit_width))
            current_in_ch = config['OUT_CH'][i]

        self.avgpool = AdaptiveAvgPool2d(1)

        self.fc = QuantLinear(in_features=current_in_ch, out_features=num_classes, bias=False,
                              weight_bit_width=weight_bit_width,
                              weight_quant_type=QUANT_TYPE)

        #for m in self.modules():
        #    if isinstance(m, QuantConv2d) or isinstance(m, QuantLinear):
        #        torch.nn.init.uniform_(m.weight.data, -1, 1)

    def forward(self, x):

        for conv_block in self.conv_blocks:
            x = conv_block(x)

        x = self.avgpool(x)

        x = x.view(x.shape[0], -1)
        x = self.fc(x)

        return x
