"""
Module for evaluating the results of a machine learning test. Specifically, the class :class:`Evaluation` is defined.
"""
from sklearn.metrics import confusion_matrix
import numpy as np
import sys


def cm_to_latex(cm, labels):
    """
    Create the latex representation of the provided confusion matrix

    :param cm: Confusion matrix to generate a latex representation for.
    :type cm: ``numpy.ndarray``
    :param labels: List of labels of the classes in the confusion matrix in the same order as they are represented in \
    the confusion matrix
    :type labels: ``list`` [``str``]
    :return: A latex representation of the confusion matrix
    :rtype: ``str``
    """
    if cm.shape[0] != len(labels):
        print('Size mismatch: provided number of labels ({}) does not correspond with the number of classes in the CM '
              '({})'.format(cm.shape[0], len(labels)))
        sys.exit(0)

    # Create preamble
    scale_def = '\\def\\scale{0.65}\n'
    preamble = '\\begin{tikzpicture}[box/.style={draw,rectangle,minimum size=1.5cm,text width=1.0cm,align=center}, ' \
               'scale=\\scale, every node/.style={scale=\\scale}]\n'

    # Create body
    indentation_factor = 1
    indentation = '    '
    body = ''

    # Define matrix
    body += indentation_factor*indentation + '\\matrix (conmat) [row sep=.1cm,column sep=.1cm] {\n'
    indentation_factor += 1

    # For every label:
    for i in range(1, len(labels) + 1):
        for j in range(1, len(labels) + 1):
            node = '\\node (m{}{}) '.format(i, j)
            properties = '[box'
            # fill = 'fill = black!30!white
            if i == 1:
                properties += ', label = above:\\textbf{{{}}}'.format(labels[j-1])

            if j == 1:
                properties += ', label = left:\\textbf{{{}}}'.format(labels[i-1])

            properties += '] '
            content = '{{{}}};'.format(cm[i-1][j-1])

            body += indentation_factor*indentation + node + properties + content

            if j == len(labels):
                body += '\\\\\n'
            else:
                body += '&\n'
    indentation_factor -= 1
    body += indentation_factor*indentation + '};\n'

    # Add axes
    axes1 = '\\node [left=.05cm of conmat,text width=1.5cm,align=right] {\\textbf{actual \\\\ value}};\n'
    axes2 = '\\node [above=.05cm of conmat] {\\textbf{prediction outcome}};\n'
    body += indentation_factor * indentation + axes1 + indentation_factor * indentation + axes2

    # Create postamble
    postamble = '\\end{tikzpicture}\n'

    return scale_def + preamble + body + postamble


def cm_dict_to_text(cm_dict):
    """
    Get a string representing the provided confusion matrix.
    {'A': {'A': 1, 'B': 2}, 'B': {'A': 3, 'B': 4}} becomes:

    +----------------------------+
    |     Predicted              |
    +----+-------+-------+-------+
    | T\ |       | **A** | **B** |
    | r\ +-------+-------+-------+
    | u\ | **A** |   1   |   2   |
    | e\ +-------+-------+-------+
    |    | **B** |   3   |   4   |
    +----+-------+-------+-------+

    :param cm_dict: Confusion matrix dictionary
    :type cm_dict: ``dict``
    :return: Text representation of the confusion.
    :rtype: ``str``
    """
    labels = list(cm_dict.keys())
    cm = np.zeros((len(labels), len(labels)), dtype=np.int)

    for truelabel_idx in range(len(labels)):
        truelabel = labels[truelabel_idx]
        for predlabel_idx in range(len(labels)):
            predlabel = labels[predlabel_idx]

            cm[truelabel_idx][predlabel_idx] = cm_dict[truelabel][predlabel]

    return cm_to_text(cm, labels=labels)


def cm_to_text(cm, labels=None):
    """
    Get a string representing the provided confusion matrix.

    :param cm: Confusion matrix
    :type cm: ``ndarray``
    :param labels: Labels corresponding with the cm
    :type labels: ``list`` of ``str``
    :return: Text representation of the confusion.
    :rtype: ``str``
    """
    if labels is None:
        labels = [str(x) for x in range(cm.shape[0])]

    max_label_width = 0
    for label in labels:
        size = len(label)
        if size > max_label_width:
            max_label_width = size

    max_label_width = max(len(str(np.max(cm))), max_label_width)

    def delimiters():
        txt = ''
        for _ in range(len(labels) + 1):
            txt += '-'.join(['' for _ in range(max_label_width + 1)]) + '+'
        return txt + '\n'

    text = '{:>{width}}|'.format('', width=max_label_width)

    for label in labels:
        text += '{:>{width}}|'.format(label, width=max_label_width)
    text += '\n' + delimiters()

    for i in range(cm.shape[0]):
        text += '{:>{width}}|'.format(labels[i], width=max_label_width)
        for j in range(cm.shape[1]):
            text += '{:>{width}}|'.format(cm[i][j], width=max_label_width)
        text += '\n' + delimiters()

    return text


def evaluation_from_cm(cm, labels=None, frac=True, decimals=2):
    """
    Turn a provided confusion matrix into an :class:`Evaluation` object.

    :param cm: Confusion matrix where each row represents a true class, and each column represents a predicted class
    :param labels: optional list with the labels belonging to each class
    :param frac: If true, data will be presented as numbers between 0 and 1 when printed
    :param decimals: Number of decimals that will be printed for a number
    :type cm: Square numpy 2d-array.
    :type labels: ``list`` of ``str``
    :type frac: ``bool``
    :type decimals: ``int``

    :return: :class:`Evaluation` object.
    :rtype: :class:`Evaluation`
    """
    # First turn confusion matrix into y_true and y_pred:
    y_true = []
    y_pred = []
    for i in range(cm.shape[0]):
        for j in range(cm.shape[0]):
            y_true += [i for _ in range(cm[i][j])]
            y_pred += [j for _ in range(cm[i][j])]
    # Then create an evaluation object
    return Evaluation(y_true, y_pred, labels=labels, frac=frac, decimals=decimals)


def evaluation_from_cm_dict(cm_dict, frac=True, decimals=2):
    """
    Extract the evaluation object from a dictionary contain a confusion matrix.

    :param cm_dict: Dictionary containing the confusion matrix
    :type cm_dict: ``dict``
    :param frac: If True, the Evaluation will represent scores in numbers between 0 and 1 rather than 0% and 100%
    :type frac: ``bool``
    :param decimals: Number of decimals to use in the Evaluation object
    :type decimals: ``int``
    :return: ``class:Evaluation`` object
    :rtype: ``Evaluation``
    """
    labels = list(cm_dict.keys())
    cm = np.zeros((len(labels), len(labels)), dtype=np.int)

    for truelabel_idx in range(len(labels)):
        truelabel = labels[truelabel_idx]
        for predlabel_idx in range(len(labels)):
            predlabel = labels[predlabel_idx]

            cm[truelabel_idx][predlabel_idx] = cm_dict[truelabel][predlabel]

    return evaluation_from_cm(cm, labels=labels, frac=frac, decimals=decimals)


def calculate_precision(tp, fp):
    """
    Calculate the precision of a result based on the number of true positives and false positives. In case of division
    by zero, the result is zero. The input can either be integers or arrays of integers.

    :param tp: Number of true positives
    :type tp: ``int`` or ``numpy.ndarray``
    :param fp: Number of false positives
    :type fp: ``int`` or ``numpy.ndarray``
    :return: The precision
    """
    return np.nan_to_num(tp / (tp + fp))


def calculate_recall(tp, fn):
    """
    Calculate the precision of a result based on the number of true positives and false negatives. In case of division
    by zero, the result is zero. The input can either be integers or arrays of integers.

    :param tp: Number of true positives
    :type tp: ``int`` or ``numpy.ndarray``
    :param fn: Number of false negatives
    :type fn: ``int`` or ``numpy.ndarray``
    :return: The recall
    """
    return np.nan_to_num(tp / (tp + fn))


def calculate_fpr(fp, tn):
    """
    Calculate the false positive rate of a result based on the number of false positives and true negatives. In case of
    division by zero, the result is zero. The input can either be integers or arrays of integers.

    :param fp: Number of false positives
    :type fp: ``int`` or ``numpy.ndarray``
    :param tn: Number of true negatives
    :type tn: ``int`` or ``numpy.ndarray``
    :return: The false positive rate
    """
    return np.nan_to_num(fp / (fp + tn))


def calculate_f1(precision, recall):
    """
    Calculate the f1-score of a result based on the specified precision and recall. In case of
    division by zero, the result is zero. The input can either be integers or arrays of integers.

    :param precision: Precision
    :type precision: ``float`` or ``numpy.ndarray``
    :param recall: Recall
    :type recall: ``float`` or ``numpy.ndarray``
    :return: The f1-score
    """
    return harmonic_mean(precision, recall)


def calculate_harmonic_mean_2_numbers(x1, x2):
    """
    Special case of the harmonic mean, where for 2 number it is equal to:
    (2 * x1 * x2) / (x1 + x2). If either of the input values is zero, the output value is also zero.

    :param x1: First input
    :type x1: ``float`` or ``numpy.ndarray``
    :param x2: Second input
    :type x2: ``float`` or ``numpy.ndarray``
    :return: Resulting harmonic
    :rtype: ``float`` or ``numpy.ndarray``
    """
    return np.nan_to_num(2 * x1 * x2 / (x1 + x2))


def harmonic_mean(*args):
    """
    Calculate the harmonic mean of an arbitrary number of inputs. If any of these inputs is zero, the output will also \
    be zero.

    :param args: ``float`` or ``numpy.ndarray``
    :return: The harmonic mean
    """
    return np.nan_to_num(len(args) / np.sum([1 / x for x in args], axis=0))


def calculate_mcc(tp, tn, fp, fn):
    """
    Calculate the Matthew's Correlation Coefficient for a binary classification scenario.


    :param tp:
    :param tn:
    :param fp:
    :param fn:
    :return:
    """

    # Start squaring here to avoid int64 overflows
    root1 = np.sqrt(tp + fp)
    root2 = np.sqrt(tp + fn)
    root3 = np.sqrt(tn + fp)
    root4 = np.sqrt(tn + fn)

    numerator = (tp * tn) - (fp * fn)
    denominator = root1 * root2 * root3 * root4
    return np.nan_to_num(numerator.astype(np.float64) / denominator)


def calculate_multiclass_mcc():
    pass


class Evaluation:
    """
    Class responsible for the calculation of evaluation metrics after testing an experiment. When presented with data,
    all relevant metrics will be calculated corresponding to conventional formulas.

    :arg y_true: True class of data samples
    :arg y_pred: Predicted class of data samples
    :arg labels: Labels corresponding to the classes. If classes are ordered 0 through n, these labels should be \
    ordered correspondingly.
    :arg frac: If True, present scores as a number between zero and 1, rather than a percentage. Default True.
    :arg decimals: Number of decimals to show when printing a report. Default 2.
    :type y_true: ``np.array`` of ``int``
    :type y_pred: ``np.array`` of ``int``
    :type labels: ``list`` of ``str``
    :type frac: ``bool``
    :type decimals: ``int``

    :Class attributes:
        * **cm** (Square numpy 2d-array.) --
          Confusion matrix containing the predicted and actual number of sanples for each class. Each row represents a \
          true class, and each column represents a predicted class.
        * **y_true** (``np.array`` of ``int``) --
          True class of data samples.
        * **y_pred** (``np.array`` of ``int``) --
          Predicted class of data samples.
        * **decimals** (``int``) --
          Number of decimals to show when printing a report.
        * **frac** (``bool``) --
          If True, present scores as a number between zero and 1, rather than a percentage.
        * **labels** (``list`` of ``str``) --
          Labels corresponding to the classes.

        * **numel** (`int``) --
          Total number of testing data samples.
        * **accuracy** (``float``) --
          Global classification accuracy as number between 0 and 1.
        * **y_pred** (``np.array`` of ``int``) --
          Predicted class of data samples.
        * **y_pred** (``np.array`` of ``int``) --
          Predicted class of data samples.
    """
    def __init__(self, y_true, y_pred, labels=None, frac=True, decimals=2):
        # Let sklearn calculate the confusion matrix
        # This will be of the form: (for binary classification)
        #     Pred
        # T   TN FP -> # Negatives
        # r   FN TP -> # Positives
        # u
        # e
        self.cm = confusion_matrix(y_true=y_true, y_pred=y_pred)
        self.y_true = y_true
        self.y_pred = y_pred

        # Representation config
        self.decimals = decimals  # Decimals to use
        self.frac = frac  # Represent values as fraction (number between 0 and 1) instead of %

        if labels is None:
            self.labels = [str(x) for x in range(self.cm.shape[0])]
        else:
            self.labels = labels

        self._calculate_metrics()

    def __str__(self):
        return self.classification_report_text()

    def _calculate_metrics(self):
        # Calculate number of elements and global accuracy
        self.numel = len(self.y_true)
        self.accuracy = np.sum(np.equal(self.y_true, self.y_pred)) / self.numel

        # Get true positives, true negatives, false negatives, false positives
        # As inspired by https://stackoverflow.com/a/43331484
        self.tp = np.diag(self.cm)
        self.tn = np.sum(np.diag(self.cm)) - self.tp
        self.fn = np.sum(self.cm, axis=1) - self.tp
        self.fp = np.sum(self.cm, axis=0) - self.tp
        self.support = np.sum(self.cm, axis=1, dtype='int')
        self.weights = self.support / np.sum(self.support)

        # Precision
        # precision = tp / (tp + fp)
        self.precision = calculate_precision(self.tp, self.fp)

        # Recall or Detection rate
        # recall = tp / (tp + fn)
        self.recall = calculate_recall(self.tp, self.fn)
        self.dr = self.recall

        # False positive rate (fpr) or False alarm rate
        # fpr = fp / (fp + tn)
        self.fpr = calculate_fpr(self.fp, self.tn)

        # F-measure
        # f1 = 2 * precision * recall / (precision + recall)
        self.f1 = calculate_f1(self.precision, self.recall)

        # Matthews correlation coefficient
        self.mcc = calculate_mcc(self.tp, self.tn, self.fp, self.fn)

        # Calculate averages:
        self._metric_labels = ['precision', 'recall', 'fpr', 'f1-score', 'mcc', 'support']
        self._metrics = np.array([self.precision, self.recall, self.fpr, self.f1, self.mcc])

        self._avg_labels = ['accuracy', 'macro avg', 'micro avg', 'weighted avg']
        self.macro_avg = np.average(self._metrics, axis=1)

        # Calculate micro averages:
        micro_precion = np.sum(self.tp) / np.sum(self.tp + self.fp)
        micro_recall = np.sum(self.tp) / np.sum(self.tp + self.fn)
        micro_fpr = np.sum(self.fp) / np.sum(self.fp + self.tn)
        micro_f1 = calculate_f1(micro_precion, micro_recall)
        micro_mcc = calculate_mcc(np.sum(self.tp), np.sum(self.tn), np.sum(self.fp), np.sum(self.fn))

        self.micro_avg = np.array([micro_precion, micro_recall, micro_fpr, micro_f1, micro_mcc])

        self.weighted_avg = np.sum(self.weights * self._metrics, axis=1)

    def classification_report(self, output_dict=False):
        """
        Get a classification report

        :param output_dict: If True, the output report will be a dictionary.
        :type output_dict: ``bool``
        :return: A classification report containing relevant classification metrics.
        :rtype: ``dict`` or ``str``
        """
        if output_dict:
            return self.classification_report_dict()
        return self.classification_report_text()

    def classification_report_dict(self):
        """
        Get a classification report in the form of a dictionary.

        :return: A classification report in the form of a dictionary.
        :rtype: ``dict``
        """
        report_dict = {}
        metric_labels = self._metric_labels
        # Metrics per class
        for label_idx in range(len(self.labels)):
            label = self.labels[label_idx]
            report_dict[label] = {}
            # Regular metrics
            for metric_idx in range(len(metric_labels) - 1):
                metric = metric_labels[metric_idx]
                report_dict[label][metric] = self._metrics[metric_idx][label_idx]
            # Support metric
            metric = metric_labels[-1]
            report_dict[label][metric] = self.support[label_idx]

        # accuracy
        report_dict[self._avg_labels[0]] = self.accuracy

        # macro avg
        report_dict[self._avg_labels[1]] = {}
        for metric_idx in range(len(metric_labels) - 1):
            metric = metric_labels[metric_idx]
            report_dict[self._avg_labels[1]][metric] = self.macro_avg[metric_idx]
        metric = metric_labels[-1]
        report_dict[self._avg_labels[1]][metric] = self.numel

        # micro avg
        report_dict[self._avg_labels[2]] = {}
        for metric_idx in range(len(metric_labels) - 1):
            metric = metric_labels[metric_idx]
            report_dict[self._avg_labels[2]][metric] = self.micro_avg[metric_idx]
        metric = metric_labels[-1]
        report_dict[self._avg_labels[2]][metric] = self.numel

        # weighted average
        report_dict[self._avg_labels[3]] = {}
        for metric_idx in range(len(metric_labels) - 1):
            metric = metric_labels[metric_idx]
            report_dict[self._avg_labels[3]][metric] = self.weighted_avg[metric_idx]
        metric = metric_labels[-1]
        report_dict[self._avg_labels[3]][metric] = self.numel
        return report_dict

    def classification_report_text(self):
        """
        Get a classification report in the form of a str.

        :return: A classification report in the form of a str.
        :rtype: ``str``
        """
        # Preliminaries
        metric_labels = self._metric_labels

        if self.frac:
            max_value_width = self.decimals + 2
        else:
            max_value_width = self.decimals + 4
        max_cell_width = max(max([len(x) for x in metric_labels]), max_value_width) + 1

        avg_labels = self._avg_labels
        max_label_width = max(max([len(x) for x in avg_labels]), max([len(x) for x in self.labels])) + 1

        if self.frac:
            factor = 1
        else:
            factor = 100
        report_lines = []
        # The classification report consists of three parts
        # Part 0: Headers
        headers = '{:>{width}}'.format('', width=max_label_width)
        for label in metric_labels:
            headers += '{:>{width}}'.format(label, width=max_cell_width)
        report_lines.append(headers)

        # Part 1: Scores per class
        metrics = np.round(self._metrics * factor, self.decimals)

        for i in range(len(self.labels)):
            line = '{:>{width}}'.format(self.labels[i], width=max_label_width)
            for m in metrics:
                line += '{:>{width}.{decimals}f}'.format(m[i], width=max_cell_width, decimals=self.decimals)
            line += '{:>{width}}'.format(self.support[i], width=max_cell_width)
            report_lines.append(line)

        # Part 2: Accuracy and averages
        # Accuracy
        accuracy = '{:>{width}}'.format(avg_labels[0], width=max_label_width)
        for _ in range(len(self.macro_avg) - 1):
            accuracy += '{:>{width}}'.format('', width=max_cell_width)
        accuracy += '{:>{width}.{decimals}f}'.format(np.round(self.accuracy * factor, self.decimals),
                                                     width=max_cell_width, decimals=self.decimals)
        accuracy += '{:>{width}}'.format(self.numel, width=max_cell_width)

        # Macro averages
        averages = '{:>{width}}'.format(avg_labels[1], width=max_label_width)
        for i in range(len(self.macro_avg)):
            averages += '{:>{width}.{decimals}f}'.format(np.round(self.macro_avg[i] * factor, self.decimals),
                                                         width=max_cell_width, decimals=self.decimals)
        averages += '{:>{width}}'.format(self.numel, width=max_cell_width)

        # Micro averages
        micaverages = '{:>{width}}'.format(avg_labels[2], width=max_label_width)
        for i in range(len(self.micro_avg)):
            micaverages += '{:>{width}.{decimals}f}'.format(np.round(self.micro_avg[i] * factor, self.decimals),
                                                            width=max_cell_width, decimals=self.decimals)
        micaverages += '{:>{width}}'.format(self.numel, width=max_cell_width)

        # Weighted averages
        waverages = '{:>{width}}'.format(avg_labels[3], width=max_label_width)
        for i in range(len(self.weighted_avg)):
            waverages += '{:>{width}.{decimals}f}'.format(np.round(self.weighted_avg[i] * factor, self.decimals),
                                                          width=max_cell_width, decimals=self.decimals)
        waverages += '{:>{width}}'.format(self.numel, width=max_cell_width)

        report_lines.append(accuracy)
        report_lines.append(averages)
        report_lines.append(micaverages)
        report_lines.append(waverages)

        return '\n'.join(report_lines)

    def cm_as_dict(self):
        """
        Get the confusion matrix as a dictionary. Each item in this dictionary is another dictionary, representing a
        TRUE row of the confusion matrix. Each row then has one value for each PREDICTED label.

        +----------------------------+
        |     Predicted              |
        +----+-------+-------+-------+
        | T\ |       | **A** | **B** |
        | r\ +-------+-------+-------+
        | u\ | **A** |   1   |   2   |
        | e\ +-------+-------+-------+
        |    | **B** |   3   |   4   |
        +----+-------+-------+-------+

        Becomes: {'A': {'A': 1, 'B': 2}, 'B': {'A': 3, 'B': 4}}

        :return: The confusion matrix as a dictionary.
        :rtype: ``dict``
        """
        cm_dict = {}
        for truelabel in range(len(self.labels)):
            cm_dict[self.labels[truelabel]] = {}
            for predlabel in range(len(self.labels)):
                cm_dict[self.labels[truelabel]][self.labels[predlabel]] = int(self.cm[truelabel][predlabel])

        return cm_dict

    def evaluate_ids(self, normal_class):
        """
        Evaluate the performance of an intrusion detection system by calculating the detection and the identification \
        scores. Provides a report containing all relevant information.

        :param normal_class: The traffic class that can be considered 'normal' or 'benign', as opposed to 'attack'. \
        Can be either
        :type normal_class: ``str`` or ``int``
        :return: Report containing
        """
        accuracy, precision, recall, fpr, f1 = self.evaluate_attack_detection(normal_class=normal_class)
        f1_micro, f1_macro = self.evaluate_attack_identification(normal_class=normal_class)
        #acc_micro2, acc_macro2 = self.evaluate_attack_identification(normal_class=normal_class, include_normal=False)

        detection_score = f1
        identification_score = calculate_harmonic_mean_2_numbers(f1_micro, f1_macro)
        #identification_score2 = calculate_harmonic_mean_2_numbers(acc_micro2, acc_macro2)

        #detection_str = 'Attack detection performance:\n'
        #detection_str += 'Accuracy: {:.2f}, Precision: {:.2f}, Recall: {:.2f}, FPR: {:.2f}, F1: {:.2f}\n'.format(
        #    accuracy * 100, precision * 100, recall * 100, fpr * 100, f1 * 100
        #)

        #identification_str = 'Attack identification performance\n'
        #identification_str += 'µAcc: {:.2f}, MAcc: {:.2f}\n'.format(acc_micro * 100, acc_macro * 100)
        #identification_str += 'µAcc no normal : {:.2f}, MAcc: {:.2f}\n'.format(acc_micro2 * 100, acc_macro2 * 100)
        #identification_str += 'Global score: {:.2f}\n'.format(identification_score * 100)
        #identification_str += 'Global score NN: {:.2f}\n'.format(identification_score2 * 100)

        #global_score = 'IDS Global score: {:.2f}'.format(calculate_harmonic_mean_2_numbers(identification_score,
        #                                                                                   detection_score) * 100)

        detection_str = 'Detection score: {:.2f}\n'.format(detection_score * 100)
        identification_str = 'Identification score: H(Fµ, FM) = H({:.2f}, {:.2f}) = {:.2f}'.format(
            f1_micro * 100, f1_macro * 100, identification_score * 100)

        return detection_str + identification_str

    def _evaluate_attack_detection(self, normal_class):
        """
                Evaluate the detection of attacks by an ids.

                :param normal_class: Class that will be considered as normal in the confusion matrix. Every other \
                class will be considered an attack. Can either be the class name (``str``) or the class index (``int``)
                :type normal_class: ``str`` or ``int``
                :return:
                """
        cm = self.multiclass_to_binary(negative_class=normal_class, approach='inclusive')

        tn = cm[0][0]
        tp = cm[1][1]
        fn = cm[1][0]
        fp = cm[0][1]

        # Accuracy
        accuracy = (tp + tn) / (tp + fp + tn + fn)

        # Precision
        # precision = tp / (tp + fp)
        precision = calculate_precision(tp, fp)

        # Recall or Detection rate
        # recall = tp / (tp + fn)
        recall = calculate_recall(tp, fn)

        # False positive rate (fpr) or False alarm rate
        # fpr = fp / (fp + tn)
        fpr = calculate_fpr(fp, tn)

        # F-measure
        # f1 = 2 * precision * recall / (precision + recall)
        f1 = calculate_f1(precision, recall)

        return accuracy, precision, recall, fpr, f1

    def evaluate_attack_detection(self, normal_class):
        if isinstance(normal_class, str):
            for i in range(len(self.labels)):
                if self.labels[i] == normal_class:
                    normal_class = i
                    break

        if len(self.labels) == 2:
            if normal_class == 0:
                attack_class = 1
            else:
                attack_class = 0
            return self.accuracy, self.precision[attack_class], self.recall[attack_class], \
                self.fpr[attack_class], self.f1[attack_class]
        else:
            return self._evaluate_attack_detection(normal_class)

    def __evaluate_attack_identification_no_normal(self, normal_class):
        """
        Evaluate the ability of identifying different attacks by first removing all samples that were predicted to be
        normal. The performance is measured by taking the micro and macro averaged accuracy scores, based on the
        accuracy scores for each attack class individually.

        :param normal_class: Class that will be considered as normal in the confusion matrix. Every other \
        class will be considered an attack. Can either be the class name (``str``) or the class index (``int``)
        :type normal_class: ``str`` or ``int``
        :return: Micro and macro averaged accuracy scores
        :rtype: ``numpy.ndarray`` and ``numpy.ndarray``
        """

        if isinstance(normal_class, str):
            for i in range(len(self.labels)):
                if self.labels[i] == normal_class:
                    normal_class = i
                    break

        # First, set all normal predictions zero:
        cm = self.cm
        cm[:, normal_class] = np.zeros(len(self.labels))

        # Then, recalculate the tp, fp, tn, fn
        tp = np.diag(self.cm)
        tn = np.sum(np.diag(self.cm)) - self.tp
        fn = np.sum(self.cm, axis=1) - self.tp
        fp = np.sum(self.cm, axis=0) - self.tp
        support = np.sum(cm, axis=1, dtype='int')

        # Calculate the f-measure for each individual class
        #accuracy = (tp + tn) / (tp + fp + tn + fn)
        precision = calculate_precision(tp, fp)
        recall = calculate_recall(tp, fn)
        accuracy = calculate_f1(precision, recall)

        # Now calculate the micro and macro scores for the accuracy
        # First, select applicable accuracies and weights
        accuracy = np.concatenate((accuracy[0:normal_class], accuracy[normal_class + 1:]), axis=0)

        support = np.concatenate((support[0:normal_class], support[normal_class + 1:]), axis=0)
        weights = support / np.sum(support)

        weighted_acc = np.sum(accuracy * weights)  # Weighted average
        macro_acc = np.mean(accuracy)  # Macro average

        return weighted_acc, macro_acc

    def evaluate_attack_identification(self, normal_class=None, include_normal=True):
        """
        Evaluate the ability to identify individual traffic (attack) classes. These classes may or may not
        include normal traffic.

        :param normal_class: Class that will be considered as normal in the confusion matrix. Every other \
        class will be considered an attack. Can either be the class name (``str``) or the class index (``int``). \
        Optional.
        :type normal_class: ``str`` or ``int``
        :param include_normal: If True, detection performance for the Normal class will be considered in the \
        identification evaluation
        :type include_normal: ``bool``
        :return: Micro and macro averaged accuracy scores
        :rtype: ``numpy.ndarray``, ``numpy.ndarray``
        """

        if include_normal:
            return self.weighted_avg[-2], self.macro_avg[-2]
        elif normal_class is not None:
            return self.__evaluate_attack_identification_no_normal(normal_class=normal_class)
        else:
            raise ValueError('Provided value for \'normal_class\'is None, but should be an integer or a string '
                             'corresponding to a traffic class.')

    def multiclass_to_binary(self, negative_class, approach='inclusive'):
        """
        Implementation of custom method to turn a multiclass confusion matrix into a binary confusion matrix.
        The specified mapping approach determines in what manner the multiclass values are considered.

        :param negative_class: Class that will be considered the negative in the binary confusion matrix. Every other \
        class will be considered positive. Can either be the class name (``str``) or the class index (``int``)
        :type negative_class: ``int`` or ``str``
        :param approach: Binary mapping approach. Must be 'inclusive', 'exclusive', 'omitting' or 'one-vs-all'.
        :type approach: ``str``
        :return: Binary confusion matrix
        """
        if isinstance(negative_class, str):
            for i in range(len(self.labels)):
                if self.labels[i] == negative_class:
                    negative_class = i
                    break

        if approach == 'inclusive':
            # Conversion to binary with the provided class as negative:
            fp = self.fn[negative_class]        # FN -> FP
            fn = self.fp[negative_class]        # FP -> FN
            tn = self.tp[negative_class]        # TP -> TN
            tp = self.numel - (fp + fn + tn)    # The rest
        elif approach == 'exclusive':
            tp = self.tn[negative_class]        # FN -> FP
            fn = self.fp[negative_class]        # FP -> FN
            tn = self.tp[negative_class]        # TP -> TN
            fp = self.numel - (tp + fn + tn)    # The rest
        elif approach == 'omitting':
            fp = self.fn[negative_class]        # FN -> FP
            fn = self.fp[negative_class]        # FP -> FN
            tn = self.tp[negative_class]        # TP -> TN
            tp = self.tn[negative_class]        # TN -> TP
        elif approach == 'one-vs-all':
            fp = self.fp[negative_class]        # Same
            fn = self.fn[negative_class]        # Same
            tn = self.tn[negative_class]        # Same
            tp = self.tp[negative_class]        # Same
        else:
            raise NotImplementedError('Specified binary mapping approach \"{}\" not supported'.format(approach))

        return np.array([[tn, fp], [fn, tp]])

    def get_cm_latex(self):
        """
        Get the confusion matrix of this Evaluation as a latex tikz figure.

        :return: Latex code for a tikz representation of the confusion matrix.
        :rtype: ``str``
        """
        return cm_to_latex(self.cm, self.labels)


if __name__ == "__main__":

    import os, json, sys
    root_dir = os.path.join('/', 'home', 'laurens', 'Experiment', 'Results for paper', 'AIHWS', 'Model performance')

    for f in os.listdir(root_dir):
        if '.json' in f:
            with open(os.path.join(root_dir, f)) as ff:
                cm = json.load(ff)
            eval = evaluation_from_cm_dict(cm, frac=False, decimals=2)
            print(f)
            print(eval)
            print(eval.evaluate_ids('BENIGN'), '\n\n')
    sys.exit(0)
    # # Binary classification test
    # y_tru = [0, 0, 0, 1, 1, 1, 1, 1, 1, 1]
    # y_pre = [0, 1, 1, 0, 0, 0, 1, 1, 1, 1]
    # e = Evaluation(y_tru, y_pre)
    # print(e.cm)
    # print(e.tp, e.tn, e.fn, e.fp)

    # 3-class classification test
    # y_tru = [0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    #          2, 2, 2, 2, 2, 2, 2, 2, 2]
    # y_pre = [0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1,
    #          2, 2, 2, 2, 2, 2, 2, 2, 2]
    # class_labels = ['class A', 'class 2', 'class NULL']
    # e = Evaluation(y_tru, y_pre, labels=class_labels, decimals=2, frac=False)
    # print(e.cm)
    # print(e.support)
    # print(e.precision)
    # print(e.recall)
    # print(e.f1)
    # print(e.averages, e.weighted_avg)
    # print(classification_report(y_tru, y_pre))
    # print(e.classification_report_text())
    # print(evaluation_from_cm(e.cm, frac=False).classification_report_text())
    # print(classification_report(y_tru, y_pre, output_dict=True))
    # print(e.classification_report_dict())
    # # Exceptional cases:
    # # Precision is undefined (tp = 0 and fp = 0)
    # y_tru = [0, 0, 0, 0, 0, 0, 0, 0]
    # y_pre = [0, 0, 0, 0, 0, 1, 1, 1]
    # e = Evaluation(y_tru, y_pre)
    # print(e.cm)
    # print(e.precision)
    # print(e.recall)
    # print(e.f1)
    # print(classification_report(y_tru, y_pre))
    # cm = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
    # print(cm)
    # e = evaluation_from_cm(cm=cm, labels=['A', 'B', 'C'])
    # print(e.classification_report_text())
    # cm_bin_1 = e.multiclass_to_binary('A', approach='inclusive')
    # cm_bin_2 = e.multiclass_to_binary('A', approach='exclusive')
    # cm_bin_3 = e.multiclass_to_binary('A', approach='omitting')
    # cm_bin_4 = e.multiclass_to_binary('A', approach='one-vs-all')
    # print(cm_bin_1)
    # print(cm_bin_2)
    # print(cm_bin_3)
    # print(cm_bin_4)

    # print(e.multiclass_to_binary('A'))
    # print(e.multiclass_to_binary('B'))
    # print(e.multiclass_to_binary('C'))
    # print(evaluation_from_cm(cm=cm2, labels=['Normal', 'Attack']).classification_report_text())
    # print(evaluation_from_cm(cm=cm2, labels=['Normal', 'Attack']).multiclass_to_binary('Normal'))
    #print(e.cm_as_dict())


    # Test
    # labels12 = ["BENIGN", "FTP", "SSH", "slowloris", "slowhttptest", "hulk", "ssgoldeneye",
    #             "heartbleed", "bruteforce", "XSS", "SQLinjection", "infiltration",
    #             "bot", "DDoS", "PortScan"]
    # cm1 = cm = np.array([[706388, 0, 0, 1, 0, 4, 2, 0, 2, 0, 0, 0, 5, 1229, 92],
    #                      [0, 2378, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    #                      [6, 0, 1811, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    #                      [8, 0, 0, 1783, 5, 0, 1, 0, 0, 2, 0, 0, 0, 0, 0],
    #                      [7, 0, 0, 0, 1676, 0, 0, 0, 3, 0, 2, 0, 0, 0, 0],
    #                      [14, 0, 0, 0, 0, 69263, 7, 0, 1, 0, 0, 0, 0, 0, 2],
    #                      [59, 0, 0, 0, 2, 2, 203, 0, 0, 0, 0, 0, 0, 1, 0],
    #                      [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    #                      [3, 0, 6, 0, 0, 0, 3, 0, 380, 73, 2, 0, 0, 0, 0],
    #                      [3, 0, 2, 0, 0, 0, 1, 0, 44, 153, 0, 0, 0, 0, 1],
    #                      [1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0],
    #                      [9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0],
    #                      [24, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 579, 0, 0],
    #                      [1541, 0, 0, 0, 0, 9, 0, 0, 0, 0, 0, 0, 0, 64011, 0],
    #                      [10, 0, 0, 0, 0, 6, 0, 0, 2, 2, 0, 0, 0, 0, 47387]]).transpose()
    # ev1 = evaluation_from_cm(cm=cm, labels=labels12)
    # cm_dict = ev1.cm_as_dict()
    # ev2 = evaluation_from_cm_dict(cm_dict)
    # print(ev1.cm)
    # print(cm_dict)
    # print(ev2.cm)


    # Features dimensionality reduction NO UDBB
    labels12 = ["BENIGN", "FTP", "SSH", "slowloris", "slowhttptest", "hulk", "ssgoldeneye",
                "heartbleed", "bruteforce", "XSS", "SQLinjection", "infiltration",
                "bot", "DDoS", "PortScan"]
    cm1 = cm = np.array([[706388, 0, 0, 1, 0, 4, 2, 0, 2, 0, 0, 0, 5, 1229, 92],
                       [0, 2378, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                       [6, 0, 1811, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                       [8, 0, 0, 1783, 5, 0, 1, 0, 0, 2, 0, 0, 0, 0, 0],
                       [7, 0, 0, 0, 1676, 0, 0, 0, 3, 0, 2, 0, 0, 0, 0],
                       [14, 0, 0, 0, 0, 69263, 7, 0, 1, 0, 0, 0, 0, 0, 2],
                       [59, 0, 0, 0, 2, 2, 203, 0, 0, 0, 0, 0, 0, 1, 0],
                       [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                       [3, 0, 6, 0, 0, 0, 3, 0, 380, 73, 2, 0, 0, 0, 0],
                       [3, 0, 2, 0, 0, 0, 1, 0, 44, 153, 0, 0, 0, 0, 1],
                       [1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0],
                       [9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0],
                       [24, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 579, 0, 0],
                       [1541, 0, 0, 0, 0, 9, 0, 0, 0, 0, 0, 0, 0, 64011, 0],
                       [10, 0, 0, 0, 0, 6, 0, 0, 2, 2, 0, 0, 0, 0, 47387]]).transpose()

    # Features dimensionality reduction WITH UDBB
    cm2 = np.array([[56642, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                   [0, 56376, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                   [0, 3, 56592, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0],
                   [0, 0, 0, 56556, 40, 0, 4, 0, 0, 0, 0, 0, 0, 0, 6],
                   [0, 0, 0, 41, 56657, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
                   [0, 0, 0, 3, 0, 56576, 7, 0, 0, 0, 0, 0, 0, 4, 25],
                   [0, 0, 0, 8, 0, 0, 56573, 0, 0, 0, 0, 0, 0, 0, 0],
                   [0, 0, 0, 0, 0, 0, 0, 56855, 0, 0, 0, 0, 0, 0, 0],
                   [0, 0, 0, 0, 0, 0, 0, 0, 53641, 2986, 112, 0, 0, 0, 0],
                   [0, 0, 0, 0, 0, 0, 0, 0, 6536, 49851, 4, 0, 0, 0, 0],
                   [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 56411, 0, 0, 0, 0],
                   [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 56436, 0, 0, 0],
                   [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 56647, 0, 0],
                   [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 56863, 0],
                   [0, 0, 0, 13, 1, 6, 2, 0, 0, 0, 0, 0, 0, 0, 156373]]).transpose()

    # DBN & density peak clustering UNSW-NB15
    labels3 = ["normal", "generic", "exploits", "fuzzers", "dos",
               "reconn", "analysis", "backdoor", "shellcode", "worms"]
    cm3 = np.array([[6131, 2, 158, 906, 18, 128, 48, 0, 9, 0],
                   [1, 3658, 97, 6, 3, 6, 0, 0, 3, 0],
                   [37, 12, 1859, 45, 230, 20, 6, 0, 14, 3],
                   [284, 0, 205, 538, 79, 80, 0, 2, 24, 0],
                   [7, 6, 589, 14, 194, 4, 0, 0, 4, 0],
                   [10, 0, 134, 4, 14, 536, 0, 0, 1, 0],
                   [0, 0, 81, 1, 52, 1, 0, 0, 0, 0],
                   [1, 0, 67, 3, 43, 1, 0, 1, 1, 0],
                   [3, 1, 16, 10, 2, 14, 0, 0, 30, 0],
                   [0, 0, 7, 1, 0, 0, 0, 0, 0, 1]])

    # A Two-Stage Classifier Approach for Network Intrusion Detection
    labels4 = ['Normal', 'Fuzzers', 'Reconnaissance', 'DoS', 'Exploits', 'Generic', 'Analysis',
               'Backdoor', 'Shellcode', 'Worms']
    cm4 = np.array([[26024, 8602, 10, 144, 344, 3, 1271, 23, 565, 14],
                   [541, 3680, 12, 1026, 299, 1, 171, 139, 183, 10],
                   [11, 21, 2927, 267, 154, 2, 14, 57, 42, 1],
                   [36, 72, 36, 2834, 540, 5, 159, 348, 56, 3],
                   [119, 218, 408, 2743, 6756, 10, 306, 464, 80, 27],
                   [10, 61, 2, 149, 399, 18205, 1, 15, 25, 4],
                   [13, 1, 0, 462, 13, 0, 118, 70, 0, 0],
                   [0, 4, 1, 384, 13, 0, 88, 93, 0, 0],
                   [3, 44, 6, 25, 34, 0, 0, 4, 262, 0],
                   [0, 0, 0, 0, 3, 1, 0, 0, 0, 40]])

    # CMs van goed naar slecht
    testcms = [(cm2, labels12), (cm1, labels12), (cm3, labels3), (cm4, labels4)]
    titles = ['Features dimensionality reduction WITH UDBB',
              'Features dimensionality reduction NO UDBB',
              'DBN & density peak clustering UNSW-NB15',
              'A Two-Stage Classifier Approach for Network Intrusion Detection']
    # for cm, labels in testcms:
    #     for approach in ['inclusive', 'exclusive', 'omitting', 'one-vs-all']:
    #         cm_bin = evaluation_from_cm(cm=cm, labels=labels).multiclass_to_binary(labels[0], approach=approach)
    #         print('{:.4f} '.format(evaluation_from_cm(cm=cm_bin, labels=['Normal', 'Attack']).f1[1]), end='')
    #     print()
    #
    # for cm, labels in testcms:
    #     for approach in ['inclusive', 'exclusive', 'omitting', 'one-vs-all']:
    #         cm_bin = evaluation_from_cm(cm=cm, labels=labels).multiclass_to_binary(labels[0], approach=approach)
    #         print('{:.4f} '.format(evaluation_from_cm(cm=cm_bin, labels=['Normal', 'Attack']).accuracy), end='')
    #     print()

    i = 0
    for cm, labels in testcms:
        evaluation = evaluation_from_cm(cm, labels=labels, frac=False)

        print(titles[i])
        print(evaluation)
        print(evaluation.evaluate_ids(labels[0]))
        i += 1


        #print(evaluation_from_cm(cm=cm_bin, labels=['Normal', 'Attack']).classification_report_text())
    # cm = np.array([[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 16]])
    # ev = evaluation_from_cm(cm=cm, labels=['Normal', 'A', 'B', 'C'])
    # print(ev.classification_report_text())
    # print(ev.evaluate_attack_identification('Normal'))
    # print(harmonic_mean(0.8, 0))
    # print(ev.evaluate_ids('Normal'))

    # Interessante testcase
    # Toward an Online Anomaly Intrusion Detection System Based on Deep Learning
    # print('\nToward an Online Anomaly Intrusion Detection System Based on Deep Learning')
    # cm = np.array([[60284, 234, 69, 0, 6],
    #                [811, 229040, 2, 0, 0],
    #                [1013, 2562, 591, 0, 0],
    #                [1611, 124, 20, 14590, 2],
    #                [65, 0, 0, 0, 5]])
    # evaluation = evaluation_from_cm(cm, labels=['Normal', 'DoS', 'Probe', 'R2L', 'U2R'], frac=False)
    # print(evaluation)
    # print(evaluation.evaluate_ids('Normal'))

    #print(cm_to_latex(cm, ['Normal', 'DoS', 'Probe', 'R2L', 'U2R']))

    #import os, json
    #cm_dict_path = os.path.join('/', 'home', 'laurens', 'Documents', 'results2', 'cm',
    #                            'cicids2017_pccn_hepa_with_benign_all_samples.json')

    #with open(cm_dict_path, 'r') as f:
    #    cm_dict = json.load(f)

    #ev2 = evaluation_from_cm_dict(cm_dict)
    #print(ev2.get_cm_latex())
    #print(evaluation_from_cm(ev2.multiclass_to_binary('BENIGN'), ['BENIGN', 'Attack']).get_cm_latex())